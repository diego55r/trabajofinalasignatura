/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import controlador.daos.ProductoDao;
import lista.controlador.Lista;
import modelo.Producto;

/**
 *
 * @author Omar Maldonado
 */
public class CtrolProducto {

    ProductoDao pd = new ProductoDao();

    /**
     *Permite modificar la lista po fuera del Dao, permite generar lista en memoria
     */
    public boolean modificar(int ubicacion, Producto aux) {

        Lista<Producto> lp = pd.listar();
        for (int i = 0; i < lp.tamanio(); i++) {
            if (ubicacion == lp.consultarDatoPosicion(i).getId()) {
                pd.modificar(aux, ubicacion);
            }

        }
        return true;
    }
    /**
     *Muestra las listas generadas en momoria
     */
    public Producto mostrar(int ubicacion) {
        System.out.println("Entra a mostrar del controlador");

        System.err.println(ubicacion);

        Producto aux = new Producto();
        Lista<Producto> lp = pd.listar();

        for (int i = 0; i < lp.tamanio(); i++) {

            if (ubicacion == lp.consultarDatoPosicion(i).getId()) {
                aux = lp.consultarDatoPosicion(ubicacion);
                System.out.println(aux.getNombre());
            }

        }

        return aux;
    }

}
