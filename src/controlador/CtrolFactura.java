/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import lista.controlador.Lista;
import modelo.Empresa;
import modelo.Factura;

/**
 *
 * @author leo23
 */
public class CtrolFactura {

    private Long dato;
//    private int cont=1;
    private String num = "";
    private Lista<Factura> listaFacturas;

    private Factura factura;

    public Factura getFactura() {
        if (factura == null) {
            factura = new Factura();
        }
        return factura;
    }

    public Lista<Factura> getListaFacturas() {
        return listaFacturas;
    }

    public void setListaFacturas(Lista<Factura> listaFacturas) {
        this.listaFacturas = listaFacturas;
    }

    public void setFactura(Factura factura) {
        this.factura = factura;
    }

    /**
     * Devuelve el string del codigo ingresado
     *
     * @param codigo cofigo de una fatura
     * @return codigo en formato String
     */
    public String formatoCodigo(Long codigo) {
        generar(codigo);

        return this.num;

    }

    /**
     * genera el string del codigo
     *
     * @param dato cagido ingresado
     */
    public void generar(Long dato) {
        this.dato = dato;
        if ((this.dato >= 10000000) || (this.dato < 100000000)) {
//               Long can=cont+this.dato;
            num = "" + this.dato;
        }
        if ((this.dato >= 1000000) || (this.dato < 10000000)) {
//               Long can=cont+this.dato;
            num = "0" + this.dato;
        }
        if ((this.dato >= 100000) || (this.dato < 1000000)) {
//               Long can=cont+this.dato;
            num = "00" + this.dato;
        }
        if ((this.dato >= 10000) || (this.dato < 100000)) {
//               Long can=cont+this.dato;
            num = "000" + this.dato;
        }
        if ((this.dato >= 1000) || (this.dato < 10000)) {
//               Long can=cont+this.dato;
            num = "0000" + this.dato;
        }
        if ((this.dato >= 100) || (this.dato < 1000)) {
//               Long can=cont+this.dato;
            num = "00000" + this.dato;
        }
        if ((this.dato >= 9) || (this.dato < 100)) {
//                Long can=cont+this.dato;
            num = "000000" + this.dato;
        }
        if (this.dato < 9) {
//               Long can=cont+this.dato;
//               num = "0000000000" + can;
            num = "0000000000" + this.dato;
        }

    }

    private Class<Factura> clazz;
    private String carpeta = "datos" + File.separatorChar;
    private Lista<Factura> lista = new Lista<>();
/**
 * Se setea una clase Factura
 */
    public CtrolFactura() {
        this.clazz = Factura.class;
        carpeta += this.clazz.getSimpleName().toLowerCase() + ".obj";
        lista.setClazz(clazz);
    }

    /**
     * guarda la lista de Facturas en un archivo obj
     *
     * @param dato objeto Factura
     * @return un boleano con el resultado
     */
    public boolean guardar(Factura dato) {
        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(carpeta));
            Lista aux = listar();
            aux.insertarNodo(dato);
            oos.writeObject(aux);
            oos.close();
            return true;
        } catch (Exception e) {
            System.out.println("Error al guardar");
        }
        return false;
    }

    /**
     * Devuelve una lista con las facturas guardadas
     *
     * @return lista de faturas
     */
    public Lista<Factura> listar() {
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(carpeta));
            lista = (Lista<Factura>) ois.readObject();
            ois.close();

        } catch (Exception e) {
            System.out.println("Error al leer el archivo");
        }
        return lista;

    }
}
