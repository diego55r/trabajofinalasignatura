
package controlador;

import lista.controlador.Lista;
import modelo.DetalleFactura;
import modelo.Producto;

/**
 *  Controlador del detalle factura
 * @author Joseph Rios
 */
public class CtrolDetalleFactura{
    private DetalleFactura df = new DetalleFactura();
    private Producto producto = new Producto();
    private Lista<Producto> listaProductos = new Lista();
/**
 * Devuelve el detalle dactura
 * @return 
 */
    public DetalleFactura getDf() {
        return df;
    }
/**
 * Recibe el detalle factura
 * @param df es un objeto DetalleFactura
 */
    public void setDf(DetalleFactura df) {
        this.df = df;
    }
/**
 * Devuele un producto 
 * @return producto previamente seteado
 */
    public Producto getProducto() {
        return producto;
    }
/**
 * Recibe un produto
 * @param producto producto a setear 
 */
    public void setProducto(Producto producto) {
        this.producto = producto;
    }
/**
 * Devuelve la lista de productos
 * @return lista de productos 
 */
    public Lista<Producto> getListaProductos() {
        return listaProductos;
    }
/**
 * Recibe una lista de productos 
 * @param listaProductos 
 */
    public void setListaProductos(Lista<Producto> listaProductos) {
        this.listaProductos = listaProductos;
    }
    
    
}
