/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.daos;

import lista.controlador.Lista;
import modelo.DetalleFactura;
import modelo.Factura;
import modelo.Producto;

/**
 *
 * @author Joseph Rios
 */
public class DetalleFacturaDao extends AdapatadoDao<DetalleFactura> {

    private DetalleFactura detalle;

    /**
     * Indica la clase Detallefactura
     */
    public DetalleFacturaDao() {
        super(DetalleFactura.class);

    }

    /**
     * Devuelve el obet detalleFatura
     *
     * @return
     */
    public DetalleFactura getDetalle() {
        if (detalle == null) {
            detalle = new DetalleFactura();
        }

        return detalle;
    }

    /**
     * Recibe el objeto detalleFactura
     *
     * @param detalle detalleFactura
     */
    public void setDetalle(DetalleFactura detalle) {
        this.detalle = detalle;
    }

    /**
     * guarda el detalleFactura
     *
     * @return el metodo de guardar factura del Adaptador
     */

    public boolean guardar() {
        detalle.setId(new Long(listar().tamanio() + 1));
        return guardar(detalle);
    }

}
