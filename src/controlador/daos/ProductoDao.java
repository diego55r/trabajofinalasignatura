/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.daos;
//utilizamos las librerias Matcher y Pattern para comprobar los correos electronico
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lista.controlador.Lista;

import modelo.Producto;

/**
 *
 * @author omar
 */
public class ProductoDao extends AdapatadoDao<Producto> {
    /**
     *Se declaran las variables
     */
    private Producto producto;
    private Lista lista;
    private final String CARPETA = "datos" + File.separatorChar + Producto.class.getSimpleName() + ".obj";
    private DetalleFacturaDao detalleFactura;
    private int contador=0;
    
    
    public ProductoDao() {
        super(Producto.class);

    }

    public String getCARPETA() {
        return CARPETA;
    }
    public Lista getLista(){
        return lista;
    }

    public Producto getProducto() {
        if (producto == null) {
            producto = new Producto();
        }

        return producto;
    }

    public void setProducto(Producto producto) {

        this.producto = producto;
    }
    /**
     *Este método permite guardar en la base local como .obj
     */
    public boolean guardarProducto() {
        Lista<Producto> aux = listar();
        try {
            this.getProducto().setId(Long.parseLong(String.valueOf(listar().tamanio() + 1)));
            aux.insertarNodo(producto);
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(CARPETA));
            oos.writeObject(aux);
            oos.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("No se pudo guardar");
            return false;
        }
    }
    

    
    //Este método nos permite buscar cualquier cadena de caracteres que nosotros queremos encontrar 
    //Ya sea nombre, apellido, etc

    public Lista<Producto> buscarString(String dato) {
        Lista<Producto> lista = new Lista<>();
        Lista<Producto> aux = listar();

        for (int i = 0; i < aux.tamanio(); i++) {
            Producto p = aux.consultarDatoPosicion(i);
            Boolean band = p.getNombre().toLowerCase().contains(dato.toLowerCase());
            if (band) {
                lista.insertarNodo(p);
            }
        }
        return lista;
    }
    
    /**
     *Permite mostrar los objetos visibles
     */
    public Lista<Producto> soloVisibles() {
        Lista<Producto> lista = new Lista<>();
        Lista<Producto> aux = listar();

        for (int i = 0; i < aux.tamanio(); i++) {
            Producto p = aux.consultarDatoPosicion(i);
            
            if (p.getVisible()) {
                lista.insertarNodo(p);
            }
        }
        return lista;
    }
    /**
     *Genera una nueva lista la cual omite la posición para luego cargar en la table
     */
    public Producto omitirPosiciones(int fila) {

        Lista<Producto> aux = listar();
        Producto p = new Producto(); 

        for (int i = 0; i <= fila; i++) {
            
            if (p.getVisible()==false) {
               i=i+1;
            }

            p = aux.consultarDatoPosicion(i); 

        }
        return p;
    }
    
    
    /**
     *Permite buscar por id
     */
    public Producto buscarPorID(Lista<Producto> a, String codigo) {
        Lista<Producto> aux = new Lista<>();
        if (a.tamanio() > 10) {
            aux = a.seleccion_clase("id", Lista.ASCENDETE);
            Producto product = aux.busqBinariaRecurListaPOS(codigo, "id", 0, aux.tamanio() - 1);
            return product;
        } else {
            for (int i = 0; i < a.tamanio(); i++) {
                System.out.println("---" + a.consultarDatoPosicion(i).getId());
                Producto p = a.consultarDatoPosicion(i);
                System.out.println("p--" + p.getId());
                System.out.println("cod-" + codigo + "---");

                if (p.getId().toString().equals(codigo)) {
                    return p;
                }

            }
        }

        return producto;
    }

   /**
     *Permite modificar un dato en la lista de productos, para luego guardarla
     */
        public Boolean modificar(){
        Lista<Producto> aux = soloVisibles();
        try {
            for (int i = 0; i < aux.tamanio(); i++) {
                if (aux.consultarDatoPosicion(i).getId().intValue() == producto.getId().intValue()) {
                    //producto=asignarIva(detalleFactura.getDetalle().getIva(), producto);
                    aux.modificarPorPos(producto, i);
                    guardarArchivo(aux);
                    break;
                }
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("No se pudo guardar");
            return false;
        }
    }
        /**
     *Guarda el archivo
     */
    private void guardarArchivo(Lista<Producto> aux){
        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(CARPETA));
            oos.writeObject(aux);
            oos.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    /**
     *Lista la información guardada en la lista
     */
    public Lista<Producto> listar() {
        Lista<Producto> lista = new Lista<>();
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(CARPETA));
            lista = (Lista<Producto>) ois.readObject();
            ois.close();
        } catch (Exception e) {
            System.out.println("Error al leer el archivo");
        }
        this.lista = lista;
        return lista;

    }
    /**
     *Genera una lista en memoria que mostrará el dáto del código
     */
    public Lista<Producto> buscarCodigo(String dato) {
        Lista<Producto> lista = new Lista<>();
        Lista<Producto> aux = soloVisibles();

        for (int i = 0; i < aux.tamanio(); i++) {
            Producto p = aux.consultarDatoPosicion(i);
            Boolean band = String.valueOf(p.getId()).contains(dato.toLowerCase());
            if (band) {
                lista.insertarNodo(p);
            }
        }
        return lista;
    }
    /**
     *Genera una lista en memoria que mostrará la busqueda por nombre
     */
     public Producto buscarPorNombre(Lista<Producto> a, String nombre) {
        Lista<Producto> aux = new Lista<>();
        if (a.tamanio() > 10) {
            aux = a.seleccion_clase("nombre", Lista.ASCENDETE);
            Producto product = aux.busqBinariaRecurListaPOS(nombre, "nombre", 0, aux.tamanio() - 1);
            return product;
        } else {
            for (int i = 0; i < a.tamanio(); i++) {
                Producto p = a.consultarDatoPosicion(i);
                if (p.getNombre().equals(nombre.toLowerCase())) {
                    return p;
                }
            }
        }
        return producto;
    }
    /**
     *Genera una nueva lista donde ya no se muestran los datos eliminados para el usuario
     */
    public Boolean modificarElim(){
        Lista<Producto> aux = soloVisibles();
        try {
            for (int i = 0; i < aux.tamanio(); i++) {
                if (aux.consultarDatoPosicion(i).getId().intValue() == producto.getId().intValue()) {
                    
                    aux.modificarPorPos(producto, i);
                    guardarArchivo(aux);
                    break;
                }
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("No se pudo guardar");
            return false;
        }
    }
    

}
