/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.daos;

import controlador.CtrolFactura;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import java.math.RoundingMode;
import javax.swing.JOptionPane;
import javax.swing.text.Document;
import javax.swing.ImageIcon;
import javax.swing.JTable;
import java.util.Calendar;

import java.awt.Desktop;
import java.awt.Toolkit;
import java.io.File;
import java.math.RoundingMode;
import lista.controlador.Lista;
import modelo.Factura;
import modelo.Producto;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import modelo.Empresa;

/**
 *
 * @author JosephR
 */
public class FacturaDao extends AdapatadoDao<Factura> {

    private Factura factura;
    private CtrolFactura ctrFactura = new CtrolFactura();
    private Empresa empresa;

    /**
     * Indica la clase Factura
     */
    public FacturaDao() {
        super(Factura.class);

    }

    /**
     * Devuelve un objeto Factura
     *
     * @return una factura
     */
    public Factura getFactura() {
        if (factura == null) {
            factura = new Factura();
        }

        return factura;
    }

    /**
     * Recibe un objeto Factura
     *
     * @param factura objeto Fatura
     */
    public void setFactura(Factura factura) {
        this.factura = factura;
    }

    /**
     * Devuelve un objeto Controlador Factura
     *
     * @return controlador factura
     */
    public CtrolFactura getCtrFactura() {
        return ctrFactura;
    }

    /**
     * Recbe un objeto Controlador facura
     *
     * @param ctrFactura controlador factura
     */
    public void setCtrFactura(CtrolFactura ctrFactura) {
        this.ctrFactura = ctrFactura;
    }

    /**
     * Devuelve un objeto Empresa
     *
     * @return objeto Empresa
     */
    public Empresa getEmpresa() {
        return empresa;
    }

    /**
     * Recibe uun objeto Empresa
     *
     * @param empresa objeto Empresa
     */
    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    /**
     * Guarda la factura o proforma en una lista
     *
     * @param esFactura ayuda a diferenciar a las facturas de las proformas
     * @return el método guardar factura del AdaptadorDao
     */
    public boolean guardar(Boolean esFactura) {
        factura.setId(new Long(listar().tamanio() + 1));
        try {
            if (esFactura) {
            factura.setNumeroFactura(numeroUltimaFactura() + 1);
        }
        } catch (Exception e) {
            factura.setNumeroFactura(new Long (1));
        }
        return guardar(factura);
    }

    /**
     * Devuelve el número de la ultima factura
     *
     * @return número factura
     */
    public Long numeroUltimaFactura() {
        try {
            int ultimaFactua = 0;
            for (int i = 0; i < listar().tamanio(); i++) {
                if (listar().consultarDatoPosicion(i).getFacturaAprobada()) {
                    ultimaFactua = i;
                }
            }
            Long numero = listar().consultarDatoPosicion(ultimaFactua).getNumeroFactura();
            return numero;
        } catch (Exception e) {

            return new Long(0);
        }

    }

    /**
     * Devuelve una lista de Facturas que contengan la cédula parametro
     *
     * @param cedula cédula buscar entre las facuras
     * @return lista de facturas con la cédula
     */
    public Lista<Factura> buscarFacturasIdentificador(String cedula) {
        Lista<Factura> lista = new Lista<>();
        Lista<Factura> aux = listar();
        for (int i = 0; i < aux.tamanio(); i++) {
            Factura p = aux.consultarDatoPosicion(i);
            Boolean band = (p.getFacturaAprobada() == true && p.getCliente().getIdentificador().equals(cedula));
            if (band) {
                lista.insertarNodo(p);
            }
        }
        return lista;
    }

    /**
     * Devuelve una lista de Facturas que contengan el codigo parametro
     *
     * @param codigo codigo a buscar entre las facuras
     * @return lista de facturas con el codigo
     */
    public Lista<Factura> buscarFacturasCodigo(Long codigo) {
        Lista<Factura> lista = new Lista<>();
        Lista<Factura> aux = listar();
        for (int i = 0; i < aux.tamanio(); i++) {
            Factura p = aux.consultarDatoPosicion(i);
            if ((p.getFacturaAprobada() == true)) {
                System.out.println("codigo: " + (codigo + p.getNumeroFactura()));
                System.out.println("codigoFactura: " + p.getNumeroFactura());
                if ((codigo + p.getNumeroFactura()) == (codigo * 2)) {
                    System.out.println("llego");
                    lista.insertarNodo(p);
                    break;
                }

            }
        }
        return lista;
    }

    /**
     * Devuelve la lista de facturas
     *
     * @return lista de todas las facturas
     */
    public Lista<Factura> buscarFacturas() {
        Lista<Factura> lista = new Lista<>();
        Lista<Factura> aux = listar();
        for (int i = 0; i < aux.tamanio(); i++) {
            Factura p = aux.consultarDatoPosicion(i);
            Boolean band = (p.getFacturaAprobada() == true);
            if (band) {
                lista.insertarNodo(p);
            }
        }
        return lista;
    }

    /**
     * Devuelve una lista de proformas que tengan la dato o identificador
     * ingresaso
     *
     * @param dato identificador del cliente
     * @return lista de proformas del cliente
     */
    public Lista<Factura> buscarProformas(String dato) {
        Lista<Factura> lista = new Lista<>();
        Lista<Factura> aux = listar();
        if (dato == null) {
            for (int i = 0; i < aux.tamanio(); i++) {
                Factura p = aux.consultarDatoPosicion(i);
                Boolean band = (p.getFacturaAprobada() != true);
                if (band) {
                    lista.insertarNodo(p);
                }
            }
        } else {
            for (int i = 0; i < aux.tamanio(); i++) {
                Factura p = aux.consultarDatoPosicion(i);
                Boolean band = (p.getFacturaAprobada() != true && p.getCliente().getIdentificador().equals(dato));
                if (band) {
                    lista.insertarNodo(p);
                }
            }
        }

        return lista;
    }

    /**
     * Devuelve la lista de clientes por su identificador
     *
     * @param dato identiicador del cliente
     * @return la lista de clientes con el indetificador
     */
    public Lista<Factura> buscarCliente(String dato) {
        Lista<Factura> lista = new Lista<>();
        Lista<Factura> aux = listar();

        for (int i = 0; i < aux.tamanio(); i++) {
            Factura p = aux.consultarDatoPosicion(i);
            Boolean band = p.getCliente().getIdentificador().contains(dato.toLowerCase());
            if (band) {
                lista.insertarNodo(p);
            }
        }
        return lista;
    }

    /**
     * Crea un pdf de la factura o proforma en una lista El achivo es guardado
     * en la carpeta del proyecto
     *
     * @param aprobada ayuda a diferenciar a las facturas de las proformas
     */
    public void imprimirDatosFactura(Boolean aprobada) {
        com.itextpdf.text.Document documento = new com.itextpdf.text.Document();

        try {
            String numeroFactura = "";
            try {
                numeroFactura = ctrFactura.formatoCodigo(factura.getNumeroFactura());

                PdfWriter.getInstance(documento, new FileOutputStream(numeroFactura + ".pdf"));
            } catch (Exception e) {

                PdfWriter.getInstance(documento, new FileOutputStream("Proforma " + factura.getId() + ".pdf"));
            }

            Image header = Image.getInstance("src/imagenes/encabezado1.jpeg");
            header.scaleToFit(650, 1000);
            header.setAlignment(Chunk.ALIGN_CENTER);

            Font encabezado = new Font();
            encabezado.setColor(BaseColor.BLACK);
            encabezado.setStyle(Font.BOLD);
            encabezado.setFamily(FontFamily.COURIER.toString());
            encabezado.setSize(21);
            Paragraph encabezadoP;
            if (aprobada) {
                encabezadoP = new Paragraph("\nFACTURA Nro: " + numeroFactura + "\n\n", encabezado);
                encabezadoP.setAlignment(Paragraph.ALIGN_CENTER);
            } else {
                encabezadoP = new Paragraph("\nPROFORMA \n\n", encabezado);
                encabezadoP.setAlignment(Paragraph.ALIGN_CENTER);
            }
            //
            Font datosEmp = new Font();
            datosEmp.setColor(BaseColor.BLACK);
            datosEmp.setFamily(FontFamily.COURIER.toString());
            datosEmp.setSize(11);
            datosEmp.setStyle(1);
            String datosEmpresa = "         Empresa: " + empresa.getNombre() + "\n" + "         Dirección: " + empresa.getDireccion()
                    + "\n         Correo: " + empresa.getCorreo() + "\n         Celular 1: " + empresa.getTelefono1()
                    + "\n         Celular 2: " + empresa.getTelefono2() + "\n         Autoriración SRI: " + empresa.getAutorizacionSRI() + "\n\n";
            Paragraph datosEmpress = new Paragraph(datosEmpresa, datosEmp);
            datosEmpress.setAlignment(Paragraph.ALIGN_LEFT);

            //datos del comprador
            Font datosC = new Font();
            datosC.setColor(BaseColor.BLACK);
            datosC.setFamily(FontFamily.COURIER.toString());
            datosC.setSize(11);
            String datosCl = "         Cliente: " + factura.getCliente().getNombre() + " " + factura.getCliente().getApellido() + "\n         C.I/RUC: " + factura.getCliente().getIdentificador()
                    + "\n" + "         Dirección: " + factura.getCliente().getDireccion() + "\n         Celular: " + factura.getCliente().getTelefono()
                    + "\n         Forma de Pago: " + factura.getFormaPago() + "\n\n";
            Paragraph datosCliente = new Paragraph(datosCl, datosC);
            datosCliente.setAlignment(Paragraph.ALIGN_LEFT);

            Font compraF = new Font();
            compraF.setColor(BaseColor.BLACK);
            compraF.setFamily(FontFamily.COURIER.toString());
            compraF.setSize(14);
            Paragraph compra = new Paragraph("COMPRA:\n\n", compraF);
            compra.setAlignment(Paragraph.ALIGN_CENTER);

            documento.open();
            documento.add(header);
            documento.add(encabezadoP);
            documento.add(datosEmpress);
            documento.add(datosCliente);
            documento.add(compra);

            PdfPTable tabla = new PdfPTable(6);

            Font encabezadoTabla = new Font();
            encabezadoTabla.setColor(BaseColor.WHITE);
            encabezadoTabla.setFamily(FontFamily.COURIER.toString());
            encabezadoTabla.setSize(14);

            PdfPCell codigo = new PdfPCell(new Phrase("CÓDIGO", encabezadoTabla));
            codigo.setBackgroundColor(BaseColor.DARK_GRAY);
            tabla.addCell(codigo);

            PdfPCell concepto = new PdfPCell(new Phrase("CONCEPTO", encabezadoTabla));
            concepto.setBackgroundColor(BaseColor.DARK_GRAY);
            tabla.addCell(concepto);

            PdfPCell unidades = new PdfPCell(new Phrase("UNIDADES", encabezadoTabla));
            unidades.setBackgroundColor(BaseColor.DARK_GRAY);
            tabla.addCell(unidades);

            PdfPCell precio = new PdfPCell(new Phrase("VALOR UNIT.", encabezadoTabla));
            precio.setBackgroundColor(BaseColor.DARK_GRAY);
            tabla.addCell(precio);

            PdfPCell subTotal = new PdfPCell(new Phrase("COSTO NETO", encabezadoTabla));
            subTotal.setBackgroundColor(BaseColor.DARK_GRAY);
            tabla.addCell(subTotal);

            PdfPCell ivaTotal = new PdfPCell(new Phrase("COSTO IVA", encabezadoTabla));
            ivaTotal.setBackgroundColor(BaseColor.DARK_GRAY);
            tabla.addCell(ivaTotal);

            Font productos = new Font();
            productos.setColor(BaseColor.BLACK);
            productos.setFamily(FontFamily.COURIER.toString());
            productos.setSize(11);

            double SubTotal = 0;
            double IVA = 0;
            double TotalIVA = 0;
            DecimalFormat df = new DecimalFormat("#.00");
            for (int i = 0; i < factura.getDetalleFactura().getProductos().tamanio(); i++) {
                tabla.addCell(new PdfPCell(new Phrase(factura.getDetalleFactura().getProductos().consultarDatoPosicion(i).getId() + "", productos)));
                tabla.addCell(new PdfPCell(new Phrase(factura.getDetalleFactura().getProductos().consultarDatoPosicion(i).getNombre(), productos)));
                tabla.addCell(new PdfPCell(new Phrase(factura.getDetalleFactura().getProductos().consultarDatoPosicion(i).getUnidades() + "", productos)));
                tabla.addCell(new PdfPCell(new Phrase(df.format(factura.getDetalleFactura().getProductos().consultarDatoPosicion(i).getPreciosiniva()), productos)));
                tabla.addCell(new PdfPCell(new Phrase(df.format(factura.getDetalleFactura().getProductos().consultarDatoPosicion(i).getPreciosiniva() * factura.getDetalleFactura().getProductos().consultarDatoPosicion(i).getUnidades()) , productos)));
                tabla.addCell(new PdfPCell(new Phrase(df.format(factura.getDetalleFactura().getProductos().consultarDatoPosicion(i).getPrecioconiva() * factura.getDetalleFactura().getProductos().consultarDatoPosicion(i).getUnidades()), productos)));

            }
            documento.add(tabla);

            SubTotal = factura.getDetalleFactura().getSubTotal();
            IVA = factura.getDetalleFactura().getIvaTotal();

            df.setRoundingMode(RoundingMode.FLOOR);

            Font totalF = new Font();
            totalF.setColor(BaseColor.BLACK);
            totalF.setFamily(FontFamily.COURIER.toString());
            totalF.setSize(14);
            String totalM = "\nSubtotal: " + df.format(SubTotal) + "       \n" + "IVA (12%): " + df.format(IVA) + "       \n" + "Total: " + df.format(factura.getDetalleFactura().getTotal()) + "       \n";
            Paragraph total = new Paragraph(totalM, totalF);

            total.setAlignment(Paragraph.ALIGN_RIGHT);

            documento.add(total);
            documento.close();
            if (aprobada) {
                JOptionPane.showMessageDialog(null, "Factura creada");
            } else {
                JOptionPane.showMessageDialog(null, "Proforma creada");
            }

        } catch (DocumentException | FileNotFoundException e) {
            System.out.println("Error en PDF " + e);
        } catch (IOException e) {
            System.out.println("Error en la imagen " + e);
        }

        try {
            File ruta = new File("C:/Documentos/ProyectoFinal_JAVA/" + ctrFactura.formatoCodigo(factura.getNumeroFactura()) + ".pdf");
            Desktop.getDesktop().open(ruta);
        } catch (Exception e) {
        }
    }
}
