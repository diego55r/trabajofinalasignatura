/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.daos;
//utilizamos las librerias Matcher y Pattern para comprobar los correos electronico
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lista.controlador.Lista;
import modelo.Persona;

/**
 *
 * @author santy
 */
public class PersonaDao extends AdapatadoDao<Persona> {

    private Persona persona;
    private Lista lista;
    private final String CARPETA = "datos" + File.separatorChar + Persona.class.getSimpleName() + ".obj";

    public PersonaDao() {
        super(Persona.class);

    }

    public String getCARPETA() {
        return CARPETA;
    }

    public Lista getLista() {
        return lista;
    }

    public Persona getPersona() {
        if (persona == null) {
            persona = new Persona();
        }

        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    /**
     * Este método permitirá guardar la lista en una carpeta especifica al mismo
     * tiempo nos va a setear un id, que servirá como identificador unico para
     * cada persona
     *
     * @return boolean
     */
    public boolean guardarPersona() {
        Lista<Persona> aux = listar();
        try {
            this.getPersona().setId(Long.parseLong(persona.getIdentificador()));
            System.out.println("guardar persona id " + getPersona().getId());
            aux.insertarNodo(persona);
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(CARPETA));
            oos.writeObject(aux);
            oos.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("No se pudo guardar");
            return false;
        }
    }

    /**
     * Este método nos permitirá actualizar la lista depues de realizar una
     * busqueda por posición del nodo al que deseamos eliminar, el método no
     * setea un nuevo id, por lo eliminará los datos en el id correspondiente
     *
     * @return
     */
    public boolean actualizarDatos(int pos) {
        Lista<Persona> aux = listar();
        try {
            aux.eliminarPorPosicion(pos);
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(CARPETA));
            oos.writeObject(aux);
            oos.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("No se pudo guardar");
            return false;
        }
    }

    /**
     * Este método nos permite buscar cualquier cadena de caracteres que
     * nosotros queremos encontrar Ya sea nombre, apellido, etc
     *
     * @param dato
     * @return lista
     */
    public Lista<Persona> buscarString(String dato) {
        System.out.println("String dato " + dato);
        Lista<Persona> lista = new Lista<>();
        Lista<Persona> aux = listar();

        for (int i = 0; i < aux.tamanio(); i++) {
            Persona p = aux.consultarDatoPosicion(i);
            Boolean band = p.getNombre().toLowerCase().contains(dato.toLowerCase());
            if (band) {
                lista.insertarNodo(p);
            }
        }
        return lista;
    }

    /**
     * Este método nos permite buscar cédula, Ruc, Pasaporte en "Frm_Clientes"
     *
     * @param dato
     * @return lista
     */
    public Lista<Persona> buscarCedula(String dato) {
        Lista<Persona> lista = new Lista<>();
        Lista<Persona> aux = listar();

        for (int i = 0; i < aux.tamanio(); i++) {
            Persona p = aux.consultarDatoPosicion(i);
            Boolean band = p.getIdentificador().contains(dato.toLowerCase());
            if (band) {
                lista.insertarNodo(p);
            }
        }
        return lista;
    }

    /**
     * Este método nos ayuda a buscar la cedula ingresada como parametro, y
     * comparará con alguna registrada en el sistema
     *
     * @param a
     * @param cedula
     * @return persona
     */
    public Persona buscarPorCedula(Lista<Persona> a, String cedula) {
        Lista<Persona> aux = new Lista<>();
        if (a.tamanio() > 10) {
            aux = a.seleccion_clase("Identificador", Lista.ASCENDETE);
            Persona persona = aux.busqBinariaRecurListaPOS(cedula, "Identificador", 0, aux.tamanio() - 1);
            return persona;
        } else {
            for (int i = 0; i < a.tamanio(); i++) {
                Persona p = a.consultarDatoPosicion(i);
                if (p.getIdentificador().equals(cedula.toLowerCase())) {
                    return p;
                }

            }
        }

        return persona;
    }

    /**
     * Este método ayuda a validar si la nueva cedula a ingresar es igual a
     * alguna ya guardada en el sistema en el caso de que sea igual, no nos
     * permitirá continuar con el método guardar
     *
     * @param a
     * @param cedula
     * @return band
     */
    public boolean CedulasSistema(Lista<Persona> a, String cedula) {
        System.out.println("Cedulas sistema " + cedula);
        Boolean band = false;
        Lista<Persona> aux = new Lista<>();
        if (a.tamanio() > 10) {
            aux = a.seleccion_clase("Identificador", Lista.ASCENDETE);
            Persona persona = aux.busqBinariaRecurListaPOS(cedula, "Identificador", 0, aux.tamanio() - 1);
            band = false;
        } else {
            for (int i = 0; i < a.tamanio(); i++) {
                Persona p = a.consultarDatoPosicion(i);
                if (p.getIdentificador().equals(cedula.toLowerCase())) {
                    System.out.println("cedula sistema " + p.getIdentificador());
                    band = true;
                }

            }
        }
        System.out.println("cedukas sistema " + band);
        return band;
    }

    /**
     * este método sirve para veritifcar si la cedula ingresada en
     * "Frm_agregarClientes" es correcta Caso contrario no nos permitirá guardar
     * los datos de cliente
     *
     * @param cedula
     * @return verificacion
     */
    public Boolean verificarCedula(String cedula) {
        System.out.println("ced personaDao " + cedula);
        boolean verificacion = false;

        int arreglo[] = {2, 1, 2, 1, 2, 1, 2, 1, 2};
        int aux[] = new int[9];

        for (int i = 0; i < aux.length; i++) {
            aux[i] = arreglo[i] * digitoCaracter(cedula, i);
            if (aux[i] > 9) {
                aux[i] = aux[i] - 9;
            }
        }
        int sumaArreglo = 0;
        for (int s = 0; s < aux.length; s++) {
            sumaArreglo = sumaArreglo + aux[s];
        }
        int decena = decenaA(sumaArreglo);

        if (decena - sumaArreglo == digitoCaracter(cedula, 9)) {
            verificacion = true;
        }
        System.out.println("Verificacion " + verificacion);
        return verificacion;
    }
    /**
     * Son método utilizados para validar cedula
     * @param cedula
     * @param k
     * @return 
     */
    public int digitoCaracter(String cedula, int k) {
        char caracter = cedula.charAt(k);
        int entero = Character.getNumericValue(caracter);
        return entero;
    }
    /**
     * método utilizado para ayudar a la validación de cedula
     * @param num
     * @return 
     */
    public int decenaA(int num) {
        int decena = (num / 10 + 1) * 10;
        return decena;
    }

    /**
     * Este método nos permite verificar en correo electronico de forma
     * "nombre@domain.com"
     *
     * @param correo
     * @return
     */
    public Boolean validarCorreo(String correo) {
        Boolean verificacion = false;
        Pattern pattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher mather = pattern.matcher(correo);
        return mather.find();
    }
    
    
    /**
     * Este método nos ayuda a modificar la lista guardada, comparando que el id, de la persona y de la posición en la lista sea la misma
     * @return 
     */
    public Boolean modificar() {
        Lista<Persona> aux = listar();
        try {
            for (int i = 0; i < aux.tamanio(); i++) {
                if (aux.consultarDatoPosicion(i).getId().intValue() == persona.getId().intValue()) {
                    aux.modificarPorPos(persona, i);
                    guardarArchivo(aux);
                    break;
                }
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("No se pudo guardar");
            return false;
        }
    }

    /**
     * Este método nos permite guardar la lista en un archivo, para poder leerlo
     * en cualquier momento
     *
     * @param aux
     */
    private void guardarArchivo(Lista<Persona> aux) {
        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(CARPETA));
            oos.writeObject(aux);
            oos.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Este método permite leer el archivo con la lista guardada para poder
     * mostar todos los datos
     *
     * @return
     */
    public Lista<Persona> listar() {
        Lista<Persona> lista = new Lista<>();
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(CARPETA));
            lista = (Lista<Persona>) ois.readObject();
            ois.close();
        } catch (Exception e) {
            System.out.println("Error al leer el archivo");
        }
        this.lista = lista;
        return lista;

    }
    
    

}
