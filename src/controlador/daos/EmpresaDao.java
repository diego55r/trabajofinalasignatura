/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.daos;

import lista.controlador.Lista;
import modelo.Empresa;
import modelo.Persona;

/**
 *
 * @author Joseph Rios
 */
public class EmpresaDao extends AdapatadoDao<Empresa> {

    private Empresa empresa;
    private Persona propietario;

    /**
     * Indica la clase factura
     */
    public EmpresaDao() {
        super(Empresa.class);

    }

    /**
     * Devuelve el objeto empresa
     *
     * @return ojeto Empresa
     */
    public Empresa getEmpresa() {
        if (empresa == null) {
            empresa = new Empresa();
        }
        return empresa;
    }

    /**
     * Recibe un objeto Empresa
     *
     * @param empresa objeto empresa
     */
    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    /**
     * Devuelve el objeto Persona propietario
     *
     * @return propietario
     */
    public Persona getPropietario() {
        if (propietario == null) {
            propietario = new Persona();
        }
        return propietario;
    }

    /**
     * Recibe el objeto perosna propietario
     *
     * @param propietario objeto persona
     */
    public void setPropietario(Persona propietario) {
        this.propietario = propietario;
    }

    /**
     * Guarda todo el objeto empresa
     *
     * @return el metodo guardar del adaptador Dao
     */
    public boolean guardar() {
        empresa.setId(new Long(listar().tamanio() + 1));
        return guardar(empresa);
    }

    /**
     * Permite modificar el objeto guardado empresa
     *
     * @return la emresa moficada
     */
    public Boolean modificar() {
        return modificar(empresa, 0);
    }

}
