/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import modelo.enums.TipoDocumento;

/**
 *
 * @author omara
 */
public class Persona implements Serializable{
    private Long id;
    private String apellido;
    private String nombre;
    private String correo;
    private String Identificador;
    private String direccion;
    private TipoDocumento tipoDocumento;
    private String telefono;
    
    

    public Persona() {
    }

    public Persona(Long id, String nombre, String correo, String identificador, String direccion, TipoDocumento tipoDocumento) {
        this.id = id;
        this.nombre = nombre;
        this.correo = correo;
        this.Identificador = identificador;
        this.direccion = direccion;
        this.tipoDocumento = tipoDocumento;
    }
    
    public Persona(Long id, String apellido, String nombre) {
        this.id = id;
        this.apellido = apellido;
        this.nombre = nombre;
      
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    
    public String getIdentificador() {
        return Identificador;
    }

    public void setIdentificador(String Identificador) {
        this.Identificador = Identificador;
    }
    
    public TipoDocumento getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(TipoDocumento tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }
    

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }


    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    

    @Override
    public String toString() {
        return apellido + " "+ nombre + " ";
    }
    
    
}
