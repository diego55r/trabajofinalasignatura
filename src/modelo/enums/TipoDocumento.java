/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo.enums;

/**
 *
 * @author santy
 */
//implementación del tipo enum
public enum TipoDocumento {
    Cedula, Ruc, Pasaporte
}
