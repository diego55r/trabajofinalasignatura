package modelo;

/**
 *
 * @author JosephR
 */
import java.io.Serializable;
import java.util.Calendar;

public class Factura implements Serializable {

    private Long id;
    private Long numeroFactura;
    private Calendar fecha;
    private Empresa empresa;

    private String formaPago;
    private Boolean facturaAprobada;
    private Persona cliente;
    private DetalleFactura detalleFactura;

    /**
     * Devuelve el objeto empresa
     *
     * @return
     */
    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    /**
     * Devuelve el objeto empresa
     *
     * @return
     */
    public DetalleFactura getDetalleFactura() {
        return detalleFactura;
    }

    public void setDetalleFactura(DetalleFactura detalleFactura) {
        this.detalleFactura = detalleFactura;
    }

    /**
     * Devuelve el objeto empresa
     *
     * @return
     */
    public String getFormaPago() {
        return formaPago;
    }

    public void setFormaPago(String formaPago) {
        this.formaPago = formaPago;
    }

    /**
     * Devuelve el objeto empresa
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     * Recibe el número de la factura
     *
     * @param id id factura
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Devuelve el número de la factura
     *
     * @return número facura
     */
    public Long getNumeroFactura() {
        return numeroFactura;
    }

    /**
     * Recibe el nuemro Facura
     *
     * @param numeroFactura número de la factura
     */
    public void setNumeroFactura(Long numeroFactura) {
        this.numeroFactura = numeroFactura;
    }

    /**
     * Devuelve el objeto Persona cliente
     *
     * @return cliente de la factura
     */
    public Persona getCliente() {
        return cliente;
    }

    /**
     * Recibe el objeto persona cliente
     *
     * @param cliente onjeto persona cliente
     */
    public void setCliente(Persona cliente) {
        this.cliente = cliente;
    }

    /**
     * Devuelve la confirmacion de la factura aprobada
     *
     * @return factura aprobada
     */
    public Boolean getFacturaAprobada() {
        return facturaAprobada;
    }

    /**
     * Recibe la aprobación de la factura
     *
     * @param facturaAprobada booleano aprobación
     */
    public void setFacturaAprobada(Boolean facturaAprobada) {
        this.facturaAprobada = facturaAprobada;
    }

    /**
     * Devuelve la fecha de la factura
     *
     * @return fecha facura
     */
    public Calendar getFecha() {
        return fecha;
    }

    /**
     * recibe el la fecha de la factura
     *
     * @param fecha fecha de la factura
     */
    public void setFecha(Calendar fecha) {
        this.fecha = fecha;
    }

}
