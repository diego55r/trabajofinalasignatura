package modelo;

import java.io.Serializable;
import java.util.Calendar;

public class Empresa implements Serializable {

    private Long id;
    private Persona pripietario;
    private String nombre;
    private String direccion;
    private String correo;
    private String telefono1;
    private String telefono2;
    private String autorizacionSRI;
    private int facturasAutorizadas;
    private Calendar fechaDeAutorizacion;
    private Calendar fechaDeCaducidad;
    private Double iva;

    /**
     * Devuelve el iva o uno predeterminado 12%
     *
     * @return iva de la empresa
     */
    public Double getIva() {
        if (iva == null) {
            return 12.0;
        } else {
            return iva;
        }
    }

    /**
     * Recibe el iva para la empresa
     *
     * @param iva iva empresa
     */
    public void setIva(Double iva) {
        this.iva = iva;
    }

    /**
     * Devuelve el objeto Persona propietario
     *
     * @return propietario de la empresa
     */
    public Persona getPripietario() {
        return pripietario;
    }

    /**
     * Recibe la persona propietario
     *
     * @param pripietario propietario de la empresa
     */
    public void setPripietario(Persona pripietario) {
        this.pripietario = pripietario;
    }

    /**
     * Devuelve el id del objeto Empresa
     *
     * @return id empresa
     */
    public Long getId() {
        return id;
    }

    /**
     * Recibe el id para la empresa
     *
     * @param id id empres
     */
    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTelefono1() {
        return telefono1;
    }

    public void setTelefono1(String telefono1) {
        this.telefono1 = telefono1;
    }

    public String getTelefono2() {
        return telefono2;
    }

    public void setTelefono2(String telefono2) {
        this.telefono2 = telefono2;
    }

    /**
     * Devuelve número de autorización SRI
     *
     * @return número de autorización SRI
     */
    public String getAutorizacionSRI() {
        return autorizacionSRI;
    }

    /**
     * Recibe número de autorización SRI
     *
     * @param autorizacionSRI número autorización SRI
     */
    public void setAutorizacionSRI(String autorizacionSRI) {
        this.autorizacionSRI = autorizacionSRI;
    }

    /**
     * Devuelve el número de facturas
     *
     * @return número de facturas
     */
    public int getFacturasAutorizadas() {
        return facturasAutorizadas;
    }

    /**
     * Recibe el número de facturas
     *
     * @param facturasAutorizadas número de facturas
     */
    public void setFacturasAutorizadas(int facturasAutorizadas) {
        this.facturasAutorizadas = facturasAutorizadas;
    }

    /**
     * Devuelve la fecha la autorizacion de las facturas
     *
     * @return fecha autorización facturas
     */
    public Calendar getFechaDeAutorizacion() {
        return fechaDeAutorizacion;
    }

    /**
     * Recibe la fecha de caducar de las facturas
     *
     * @param fechaDeAutorizacion fecha facturas autorizadas
     */
    public void setFechaDeAutorizacion(Calendar fechaDeAutorizacion) {
        this.fechaDeAutorizacion = fechaDeAutorizacion;
    }

    /**
     * Devuelve la fecha de caducidad de las facturas
     *
     * @return fecha de caducidad facturas
     */
    public Calendar getFechaDeCaducidad() {
        return fechaDeCaducidad;
    }

    /**
     * Recibe la fecha de caducar de las facturas
     *
     * @param fechaDeCaducidad fecha facturas caducan
     */
    public void setFechaDeCaducidad(Calendar fechaDeCaducidad) {
        this.fechaDeCaducidad = fechaDeCaducidad;
    }

}
