package modelo;

import java.io.Serializable;
import lista.controlador.Lista;

/**
 *
 * @author JosephR
 */
public class DetalleFactura implements Serializable {

    private Long id;
    private String numeroFactura;

    private Lista<Producto> productos;

    private Double subTotal;
    private Double ivaTotal;
    private Double total;
    private Double iva;

    /**
     * Devueleve el iva o un valor predeterminado 12%
     *
     * @return iva del detalle factura
     */
    public Double getIva() {
        if (iva == null) {
            return 12.0;
        } else {
            return iva;
        }
    }

    /**
     * Recibe el iva
     *
     * @param iva iva a coloacar
     */
    public void setIva(Double iva) {
        this.iva = iva;
    }

    /**
     * Devuelve el subtotal
     *
     * @return subtotal del detalle
     */
    public Double getSubTotal() {
        return subTotal;
    }

    /**
     * Recibe el subtotal para el detalle
     *
     * @param subTotal subtotal del detalle
     */
    public void setSubTotal(Double subTotal) {
        this.subTotal = subTotal;
    }

    /**
     * Devuelve el ivaTotal
     *
     * @return ivaTotal del detalle
     */
    public Double getIvaTotal() {
        return ivaTotal;
    }

    /**
     * Recibe el ivaTotal para el detalle
     *
     * @param ivaTotal iva total
     */
    public void setIvaTotal(Double ivaTotal) {
        this.ivaTotal = ivaTotal;
    }

    /**
     * Devuelve el valor total del detalle
     *
     * @return valor total
     */
    public Double getTotal() {
        return total;
    }

    /**
     * Recibe el total para el detalle
     *
     * @param total total del detalle
     */
    public void setTotal(Double total) {
        this.total = total;
    }

    /**
     * id del detalle
     *
     * @return ide datelle
     */
    public Long getId() {
        return id;
    }

    /**
     * Recibe el ide para el detalle
     *
     * @param id id a recibir
     */

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * devuelve el numero factura
     *
     * @return numero factura
     */
    public String getNumeroFactura() {
        return numeroFactura;
    }

    /**
     * Recibe el nuermo de la factura
     *
     * @param numeroFactura
     */
    public void setNumeroFactura(String numeroFactura) {
        this.numeroFactura = numeroFactura;
    }

    /**
     * Devuelve la lista de productos del detalle
     *
     * @return lista de prorductos
     */
    public Lista<Producto> getProductos() {
        return productos;
    }

    /**
     * Recibe la lista de productos
     *
     * @param productos lista de productos
     */
    public void setProductos(Lista<Producto> productos) {
        this.productos = productos;
    }

}
