/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista.modelo;

import javax.swing.table.AbstractTableModel;
import lista.controlador.Lista;
import modelo.Producto;

/**
 *
 * @author omara
 */
public class tablaProducto extends AbstractTableModel {
     private Lista<Producto> lista= new Lista();

    public Lista<Producto> getLista() {
        return lista;
    }

    public void setLista(Lista<Producto> lista) {
        this.lista = lista;
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public int getRowCount() {
        return lista.tamanio();
    }

    @Override
    public String getColumnName(int column) {
        switch(column){
            case 0: return "Codigo";
            case 1: return "Nombre";
            case 2: return "Detalle";
            case 3: return "Costo Neto";
            case 4: return "Costo con Iva";

            default: return null;
           
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Producto p= lista.consultarDatoPosicion(rowIndex);
        switch(columnIndex){
            case 0: return p.getId();
            case 1: return p.getNombre();
            case 2: return p.getDescripcion();
            case 3: return p.getPreciosiniva();
            case 4: return p.getPrecioconiva();
          
            default: return null;
           
        }
    }
}
