/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista.modelo;

import controlador.CtrolFactura;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.swing.table.AbstractTableModel;
import lista.controlador.Lista;
import modelo.Factura;
import modelo.Producto;

/**
 *
 * @author omara
 */
public class tablaFacturas extends AbstractTableModel {
     private Lista<Factura> lista= new Lista();
       private CtrolFactura cf = new CtrolFactura();
    public Lista<Factura> getLista() {
        return lista;
    }

    public void setLista(Lista<Factura> lista) {
        this.lista = lista;
    }

    
    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public int getRowCount() {
        try {
            return lista.tamanio();
        } catch (Exception e) {
        return 0;
        }
        
    }

    @Override
    public String getColumnName(int column) {
        switch(column){
            case 0: return "Nro";
            case 1: return "Número Factura";
            case 2: return "Fecha";
            case 3: return "Cliente";
            case 4: return "Productos";
            case 5: return "Total";

            default: return null;
           
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Factura fact= lista.consultarDatoPosicion(rowIndex);
        if(fact.getFacturaAprobada()){
        switch(columnIndex){
            case 0: return rowIndex+1;
            case 1: return cf.formatoCodigo(fact.getNumeroFactura());
            case 2: return formatoFecha(fact.getFecha());
            case 3: return fact.getCliente().getNombre() + " " + fact.getCliente().getApellido();
            case 4: return productos(fact);
            case 5: return fact.getDetalleFactura().getTotal() ;//+ "/"+total(fact);

            default: return null;
        }
        }
        return null;
    }
    
    public String productos(Factura fact){
    String prod="";
        for (int i = 0; i < fact.getDetalleFactura().getProductos().tamanio(); i++) {
            prod+=fact.getDetalleFactura().getProductos().consultarDatoPosicion(i).getNombre()+" ";
        }
    
    return prod;
    }
    
//    public String total(Factura fact) {
//        Double Total = 0.0;
//        DecimalFormat df = new DecimalFormat("###.##");
//
//        for (int i = 0; i < fact.getDetalleFactura().getProductos().tamanio(); i++) {
//            Total += fact.getDetalleFactura().getProductos().consultarDatoPosicion(i).getPrecioconiva();
//            
//        }
//        return df.format(Total);
//
//    }
    public String formatoFecha(Calendar fecha) {
//        SimpleDateFormat formato = new SimpleDateFormat("dd MMM yyyy hh:mm");
        SimpleDateFormat formato = new SimpleDateFormat("yyyy/MM/dd");
        String fechaF = formato.format(fecha.getTime());
        return fechaF;
    }
    public String valor(Double valor) {
        DecimalFormat df = new DecimalFormat("###.##");
        return df.format(valor);

    }
}
