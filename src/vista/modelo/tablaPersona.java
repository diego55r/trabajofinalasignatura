/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista.modelo;

import javax.swing.table.AbstractTableModel;
import lista.controlador.Lista;
import modelo.Persona;

/**
 *
 * @author santy
 */
public class tablaPersona extends AbstractTableModel {
    private Lista<Persona> lista= new Lista();

    public Lista<Persona> getLista() {
        return lista;
    }

    public void setLista(Lista<Persona> lista) {
        this.lista = lista;
    }

    @Override
    public int getColumnCount() {
        return 7;
    }

    @Override
    public int getRowCount() {
        return lista.tamanio();
    }

    @Override
    public String getColumnName(int column) {
        switch(column){
            case 0: return "Nro";
            case 1: return "Nombres";
            case 2: return "Apellidos";
            case 3: return "Tipo de documento";
            case 4: return "Nro Documento";
            case 5: return "Correo";
            case 6: return "Dirección";
            case 7: return "Telefono";
            default: return null;
           
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Persona p= lista.consultarDatoPosicion(rowIndex);
        switch(columnIndex){
            case 0: return rowIndex+1;
            case 1: return p.getNombre();
            case 2: return p.getApellido();
            case 3: return p.getTipoDocumento();
            case 4: return p.getIdentificador();
            case 5: return p.getCorreo();
            case 6: return p.getDireccion();
            case 7: return p.getTelefono();
            default: return null;
           
        }
    }
}
