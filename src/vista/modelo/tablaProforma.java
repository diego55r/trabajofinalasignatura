/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista.modelo;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.swing.table.AbstractTableModel;
import lista.controlador.Lista;
import modelo.Factura;
import modelo.Producto;

/**
 *
 * @author omara
 */
public class tablaProforma extends AbstractTableModel {

    private Lista<Factura> lista = new Lista();
    private Boolean estado;

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public Lista<Factura> getLista() {
        return lista;
    }

    public void setLista(Lista<Factura> lista) {
        this.lista = lista;
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public int getRowCount() {
        try {
            return lista.tamanio();
        } catch (Exception e) {
            return 0;
        }

    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Nro";
            case 1:
                return "Fecha";
            case 2:
                return "Identificador";
            case 3:
                return "Nombre Cliente";
            case 4:
                return "Productos";
            case 5:
                return "Total";

            default:
                return null;

        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Factura fact = lista.consultarDatoPosicion(rowIndex);
        if (!fact.getFacturaAprobada()) {
            switch (columnIndex) {
                case 0:
                    return rowIndex + 1;
                case 1:
                    return formatoFecha(fact.getFecha());
                case 2:
                    return fact.getCliente().getIdentificador();
                case 3:
                    return fact.getCliente().getNombre() + " " + fact.getCliente().getApellido();
                case 4:
                    return productos(fact);
                case 5:
                    return valor(fact.getDetalleFactura().getTotal());

                default:
                    return null;
            }
        }
        return null;
    }

    public String productos(Factura fact) {
        String prod = "";
        for (int i = 0; i < fact.getDetalleFactura().getProductos().tamanio(); i++) {
            prod += fact.getDetalleFactura().getProductos().consultarDatoPosicion(i).getNombre() + " ";
        }

        return prod;
    }

    public String valor(Double valor) {
        DecimalFormat df = new DecimalFormat("###.##");
        return df.format(valor);

    }

    public String formatoFecha(Calendar fecha) {
        SimpleDateFormat formato = new SimpleDateFormat("yyyy/MM/dd");
        String fechaF = formato.format(fecha.getTime());
        return fechaF;
    }
}
