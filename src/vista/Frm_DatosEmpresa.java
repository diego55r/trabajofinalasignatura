/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import controlador.daos.CuentaDao;
import controlador.daos.EmpresaDao;
import controlador.daos.PersonaDao;
import java.util.Calendar;
import javax.swing.JOptionPane;
import modelo.Persona;
import modelo.enums.TipoDocumento;

/**
 *
 * @author compa
 */
public class Frm_DatosEmpresa extends javax.swing.JFrame {
    /**
     * Creates new form Frm_agregarProductos
     */
    private PersonaDao personadao = new PersonaDao();
    private EmpresaDao empDao = new EmpresaDao();

    public Frm_DatosEmpresa() {
        initComponents();
        if (empDao.listar().tamanio() > 0) {
            mostrarDatos();
        }
    }

//    private void bloquearIngreso() {
//        txtNombreEMP.setEnabled(false);
//        txtCorreoEMP.setEnabled(false);
//        txtDireccionEMP.setEnabled(false);
//        txtNombreEMP.setEnabled(false);
//        txtTelefonoEMP.setEnabled(false);
//        txtTelefonoEMP2.setEnabled(false);
//        radAutorizacionSri.setEnabled(false);
//        txtFechaAutorizacion.setEnabled(false);
//        txtFechaCaducidad.setEnabled(false);
//
//        txtCorreo.setEnabled(false);
//        txtDireccion.setEnabled(false);
//        txtNombre.setEnabled(false);
//        txtApellido.setEnabled(false);
//        txtTelefono.setEnabled(false);
//        txtIdentificacion.setEnabled(false);
//        txtIva.setEnabled(false);
//        spinUnidadesFActuras.setEnabled(false);
//    }
//
//    private void desBloquearIngreso() {
//        txtNombreEMP.setEnabled(true);
//        txtCorreoEMP.setEnabled(true);
//        txtDireccionEMP.setEnabled(true);
//        txtNombreEMP.setEnabled(true);
//        txtTelefonoEMP.setEnabled(true);
//        txtTelefonoEMP2.setEnabled(true);
//        radAutorizacionSri.setEnabled(true);
//        txtFechaAutorizacion.setEnabled(true);
//        txtFechaCaducidad.setEnabled(true);
//
//        txtCorreo.setEnabled(true);
//        txtDireccion.setEnabled(true);
//        txtNombre.setEnabled(true);
//        txtApellido.setEnabled(true);
//        txtTelefono.setEnabled(true);
//        txtIdentificacion.setEnabled(true);
//        txtIva.setEnabled(true);
//        spinUnidadesFActuras.setEnabled(true);
//    }
    private void mostrarDatos() {
        try {
            empDao.setEmpresa(empDao.listar().consultarDatoPosicion(0));
            txtNombreEMP.setText(empDao.getEmpresa().getNombre());
            txtCorreoEMP.setText(empDao.getEmpresa().getCorreo());
            txtDireccionEMP.setText(empDao.getEmpresa().getDireccion());
            txtNombreEMP.setText(empDao.getEmpresa().getNombre());
            txtTelefonoEMP.setText(empDao.getEmpresa().getTelefono1());
            txtTelefonoEMP2.setText(empDao.getEmpresa().getTelefono2());
            txtAutorizacion.setText(empDao.getEmpresa().getAutorizacionSRI());
            txtFechaAutorizacion.setCalendar(empDao.getEmpresa().getFechaDeAutorizacion());
            txtFechaCaducidad.setCalendar(empDao.getEmpresa().getFechaDeCaducidad());
            txtIva.setText(empDao.getEmpresa().getIva() + "");
            spinUnidadesFActuras.setValue(empDao.getEmpresa().getFacturasAutorizadas());
            txtCorreo.setText(empDao.getEmpresa().getPripietario().getCorreo());
            txtDireccion.setText(empDao.getEmpresa().getPripietario().getDireccion());
            txtNombre.setText(empDao.getEmpresa().getPripietario().getNombre());
            txtApellido.setText(empDao.getEmpresa().getPripietario().getApellido());
            txtTelefono.setText(empDao.getEmpresa().getPripietario().getTelefono());
            txtIdentificacion.setText(empDao.getEmpresa().getPripietario().getIdentificador());
        } catch (Exception e) {
        }

    }

    private void limpiarDatos() {
        if (!(empDao.listar().tamanio() > 0)) {
            txtNombreEMP.setText("");
            txtCorreoEMP.setText("");
            txtDireccionEMP.setText("");
            txtNombreEMP.setText("");
            txtTelefonoEMP.setText("");
            txtTelefonoEMP2.setText("");
            txtAutorizacion.setText("");
            txtFechaAutorizacion.setCalendar(null);
            txtFechaCaducidad.setCalendar(null);
            txtIva.setText("");
            spinUnidadesFActuras.setValue(0);
            txtCorreo.setText("");
            txtDireccion.setText("");
            txtNombre.setText("");
            txtApellido.setText("");
            txtTelefono.setText("");
            txtIdentificacion.setText("");
        }

    }

    ///setaer datos sss
    private void setearDatos() {
        empDao.getEmpresa().setCorreo(txtCorreoEMP.getText().trim());
        empDao.getEmpresa().setDireccion(txtDireccionEMP.getText().trim());
        empDao.getEmpresa().setFacturasAutorizadas((Integer) spinUnidadesFActuras.getValue());
        empDao.getEmpresa().setFechaDeAutorizacion(txtFechaAutorizacion.getCalendar());
        empDao.getEmpresa().setFechaDeCaducidad(txtFechaCaducidad.getCalendar());
        empDao.getEmpresa().setTelefono1(txtTelefonoEMP.getText().trim());
        empDao.getEmpresa().setTelefono2(txtTelefonoEMP2.getText().trim());
        empDao.getEmpresa().setNombre(txtNombreEMP.getText().trim());

        empDao.getEmpresa().setAutorizacionSRI(txtAutorizacion.getText().trim());
        empDao.getEmpresa().setPripietario(empDao.getPropietario());
        empDao.getEmpresa().getPripietario().setApellido(txtApellido.getText().trim());
        empDao.getEmpresa().getPripietario().setNombre(txtNombre.getText().trim());
        empDao.getEmpresa().getPripietario().setCorreo(txtCorreo.getText().trim());
        empDao.getEmpresa().getPripietario().setDireccion(txtDireccion.getText());
        empDao.getEmpresa().getPripietario().setTelefono(txtTelefono.getText().trim());
        empDao.getEmpresa().getPripietario().setIdentificador(txtIdentificacion.getText().trim());
    }

    private void guardar() {
        int opcion;
        if (validar() && empDao.listar().tamanio() == 0) {
            setearDatos();
            opcion = JOptionPane.showConfirmDialog(null, "¿Está seguro de guardar los datos actuales?");
            if (opcion == 0) {
                if (empDao.guardar()) {
                    JOptionPane.showMessageDialog(null, "Guardado con exito", "Ok", JOptionPane.INFORMATION_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(null, "No se puede guardar", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }

    private void actualizar() {
        int opcion;
        if (empDao.listar().tamanio() > 0) {
            setearDatos();
            opcion = JOptionPane.showConfirmDialog(null, "¿Está seguro de actualizar los datos actuales?");
            if (opcion == 0) {
                if (empDao.modificar()) {
                    JOptionPane.showMessageDialog(null, "Se ha modificado correctamente", "OK", JOptionPane.WARNING_MESSAGE);
                    mostrarDatos();
                } else {
                    JOptionPane.showMessageDialog(null, "Hubo un error al modificar", "Error", JOptionPane.ERROR_MESSAGE);
                }
            } else {

            }
        } else {
            JOptionPane.showMessageDialog(null, "No hay datos a actualizar", "Información", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    private boolean validar() {
        Boolean campos = false;
        if (txtCorreoEMP.getText().trim().length() > 0
                && txtDireccionEMP.getText().trim().length() > 0
                && txtNombreEMP.getText().trim().length() > 0
                && txtTelefonoEMP.getText().trim().length() > 0
                && txtTelefonoEMP2.getText().trim().length() > 0
                && txtAutorizacion.getText().trim().length() > 0
                && txtApellido.getText().trim().length() > 0
                && txtCorreo.getText().trim().length() > 0
                && txtDireccion.getText().trim().length() > 0
                && txtFechaAutorizacion.getDateFormatString().trim().length() > 0
                && txtFechaCaducidad.getDateFormatString().trim().length() > 0
                && txtIdentificacion.getText().trim().length() > 0
                && txtNombre.getText().trim().length() > 0
                && txtTelefono.getText().trim().length() > 0) {
            campos = true;

        } else {
            JOptionPane.showMessageDialog(null, "Llene todos los campos", "Error", JOptionPane.ERROR_MESSAGE);
        }

        return campos && selecionarIva() && selecionarUnidades() && validarFechas() && validarCorreos();
    }

    private Boolean validarCorreos() {
        Boolean band1 = false;
        Boolean band2 = false;
        if (personadao.validarCorreo(txtCorreoEMP.getText().trim())) {
            band1 = true;
        } else {
            JOptionPane.showMessageDialog(null, "Ingrese el correo de la empresa correctamente", "Información", JOptionPane.INFORMATION_MESSAGE);

        }
        if (personadao.validarCorreo(txtCorreoEMP.getText().trim())) {
            band2 = true;
        } else {
            JOptionPane.showMessageDialog(null, "Ingrese el correo del propietario de la empresa correctamente", "Información", JOptionPane.INFORMATION_MESSAGE);

        }
        return band1 && band2;
    }

    public Boolean selecionarUnidades() {
        try {
            spinUnidadesFActuras.commitEdit();
        } catch (java.text.ParseException e) {
        }
        int unidades = (Integer) spinUnidadesFActuras.getValue();
        System.out.println("unidades + " + unidades);
        try {
            if (unidades > 0) {
                empDao.getEmpresa().setFacturasAutorizadas(unidades);
                return true;
            } else {
                JOptionPane.showMessageDialog(null, "Seleccione el número de facuras", "Información", JOptionPane.INFORMATION_MESSAGE);
                return false;
            }
        } catch (Exception e) {
        }
        return false;
    }

    public Boolean selecionarIva() {

        try {
            Double iva = Double.parseDouble(txtIva.getText().trim());
            System.out.println("iva + " + iva);

            if (iva >= 0) {
                empDao.getEmpresa().setIva(iva);
                return true;
            } else {
                JOptionPane.showMessageDialog(null, "Digite el iva general para los productos", "Información", JOptionPane.INFORMATION_MESSAGE);
            }
        } catch (Exception e) {
            return false;
        }

        return false;
    }

    private Boolean validarFechas() {
        Calendar cal1 = txtFechaAutorizacion.getCalendar();
        Calendar cal2 = txtFechaCaducidad.getCalendar();
        if (cal1.before(cal2)) {
            return true;
        } else {
            JOptionPane.showMessageDialog(null, "Ingrese correctamente las fecha", "Información", JOptionPane.INFORMATION_MESSAGE);
            return false;
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel15 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        txtCorreoEMP = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        btn_guardarPersona = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        btn_cancelarPersona = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        btn_irFactura1 = new javax.swing.JLabel();
        txtDireccionEMP = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        txtTelefonoEMP = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        txtNombreEMP = new javax.swing.JTextField();
        jPanel9 = new javax.swing.JPanel();
        btn_actualizarPersona = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        txtTelefonoEMP2 = new javax.swing.JTextField();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        txtFechaAutorizacion = new com.toedter.calendar.JDateChooser();
        txtFechaCaducidad = new com.toedter.calendar.JDateChooser();
        jLabel14 = new javax.swing.JLabel();
        txtIdentificacion = new javax.swing.JTextField();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        txtDireccion = new javax.swing.JTextField();
        jLabel29 = new javax.swing.JLabel();
        txtTelefono = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        txtCorreo = new javax.swing.JTextField();
        jLabel30 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        spinUnidadesFActuras = new javax.swing.JSpinner();
        txtIva = new javax.swing.JTextField();
        jLabel33 = new javax.swing.JLabel();
        txtAutorizacion = new javax.swing.JTextField();
        txtApellido = new javax.swing.JTextField();

        jLabel2.setFont(new java.awt.Font("Roboto", 1, 18)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("PRODUCTOS");

        jLabel3.setFont(new java.awt.Font("Roboto", 1, 18)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("PRODUCTOS");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        jPanel2.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, 610, 10));

        jLabel15.setFont(new java.awt.Font("Roboto", 1, 18)); // NOI18N
        jLabel15.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel15.setText("DATOS EMPRESA IKONOS");
        jPanel2.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 10, 270, 20));

        jLabel17.setFont(new java.awt.Font("Roboto", 1, 14)); // NOI18N
        jLabel17.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel17.setText("Correo:");
        jPanel2.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 100, 110, 20));

        txtCorreoEMP.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        txtCorreoEMP.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jPanel2.add(txtCorreoEMP, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 100, 160, -1));

        jPanel4.setBackground(new java.awt.Color(0, 102, 255));
        jPanel4.setForeground(new java.awt.Color(0, 51, 255));

        btn_guardarPersona.setFont(new java.awt.Font("Roboto", 1, 11)); // NOI18N
        btn_guardarPersona.setForeground(new java.awt.Color(255, 255, 255));
        btn_guardarPersona.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btn_guardarPersona.setText("GUARDAR");
        btn_guardarPersona.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btn_guardarPersona.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_guardarPersonaMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btn_guardarPersona, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btn_guardarPersona, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel2.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 270, 120, 30));

        jPanel7.setBackground(new java.awt.Color(0, 102, 255));
        jPanel7.setForeground(new java.awt.Color(0, 51, 255));

        btn_cancelarPersona.setFont(new java.awt.Font("Roboto", 1, 11)); // NOI18N
        btn_cancelarPersona.setForeground(new java.awt.Color(255, 255, 255));
        btn_cancelarPersona.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btn_cancelarPersona.setText("CANCELAR");
        btn_cancelarPersona.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btn_cancelarPersona.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_cancelarPersonaMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btn_cancelarPersona, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btn_cancelarPersona, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel2.add(jPanel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 270, -1, -1));

        jPanel8.setBackground(new java.awt.Color(0, 102, 255));
        jPanel8.setForeground(new java.awt.Color(0, 51, 255));

        btn_irFactura1.setFont(new java.awt.Font("Roboto", 1, 11)); // NOI18N
        btn_irFactura1.setForeground(new java.awt.Color(255, 255, 255));
        btn_irFactura1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btn_irFactura1.setText("FACTURAR");
        btn_irFactura1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btn_irFactura1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_irFactura1MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btn_irFactura1, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btn_irFactura1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel2.add(jPanel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 310, -1, -1));

        txtDireccionEMP.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        jPanel2.add(txtDireccionEMP, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 70, 160, -1));

        jLabel19.setFont(new java.awt.Font("Roboto", 1, 14)); // NOI18N
        jLabel19.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel19.setText("Teléfono 1:");
        jPanel2.add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 130, 110, 20));

        txtTelefonoEMP.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        jPanel2.add(txtTelefonoEMP, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 130, 160, -1));

        jLabel21.setFont(new java.awt.Font("Roboto", 1, 14)); // NOI18N
        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel21.setText("Dirección:");
        jPanel2.add(jLabel21, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 70, 110, 20));

        txtNombreEMP.setText("IKONOS");
        txtNombreEMP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNombreEMPActionPerformed(evt);
            }
        });
        jPanel2.add(txtNombreEMP, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 40, 160, -1));

        jPanel9.setBackground(new java.awt.Color(0, 102, 255));
        jPanel9.setForeground(new java.awt.Color(0, 51, 255));

        btn_actualizarPersona.setFont(new java.awt.Font("Roboto", 1, 11)); // NOI18N
        btn_actualizarPersona.setForeground(new java.awt.Color(255, 255, 255));
        btn_actualizarPersona.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btn_actualizarPersona.setText("ACTUALIZAR");
        btn_actualizarPersona.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btn_actualizarPersona.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_actualizarPersonaMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btn_actualizarPersona, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btn_actualizarPersona, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel2.add(jPanel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 310, 120, -1));

        jLabel22.setFont(new java.awt.Font("Roboto", 1, 14)); // NOI18N
        jLabel22.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel22.setText("Caducidad:");
        jPanel2.add(jLabel22, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 310, 110, 20));

        txtTelefonoEMP2.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        jPanel2.add(txtTelefonoEMP2, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 160, 160, -1));

        jLabel24.setFont(new java.awt.Font("Roboto", 1, 14)); // NOI18N
        jLabel24.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel24.setText("Autorizacion SRI:");
        jPanel2.add(jLabel24, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 190, 130, 20));

        jLabel25.setFont(new java.awt.Font("Roboto", 1, 14)); // NOI18N
        jLabel25.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel25.setText("Autorización:");
        jPanel2.add(jLabel25, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 280, 110, 20));

        jLabel20.setFont(new java.awt.Font("Roboto", 1, 14)); // NOI18N
        jLabel20.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel20.setText("Nombre:");
        jPanel2.add(jLabel20, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 40, 110, 20));

        jLabel26.setFont(new java.awt.Font("Roboto", 1, 14)); // NOI18N
        jLabel26.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel26.setText("Nombre:");
        jPanel2.add(jLabel26, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 40, 110, 20));

        txtNombre.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        txtNombre.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        jPanel2.add(txtNombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 40, 160, 28));
        jPanel2.add(txtFechaAutorizacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 280, 160, -1));
        jPanel2.add(txtFechaCaducidad, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 310, 160, 28));

        jLabel14.setFont(new java.awt.Font("Roboto", 1, 14)); // NOI18N
        jLabel14.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel14.setText("RUC:");
        jPanel2.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 100, 120, 20));

        txtIdentificacion.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        jPanel2.add(txtIdentificacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 100, 160, -1));

        jLabel27.setFont(new java.awt.Font("Roboto", 1, 14)); // NOI18N
        jLabel27.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel27.setText("Apellido:");
        jPanel2.add(jLabel27, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 70, 110, 20));

        jLabel28.setFont(new java.awt.Font("Roboto", 1, 14)); // NOI18N
        jLabel28.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel28.setText("Dirección:");
        jPanel2.add(jLabel28, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 130, 110, 20));

        txtDireccion.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        jPanel2.add(txtDireccion, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 130, 160, -1));

        jLabel29.setFont(new java.awt.Font("Roboto", 1, 14)); // NOI18N
        jLabel29.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel29.setText("Teléfono:");
        jPanel2.add(jLabel29, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 160, 110, 20));

        txtTelefono.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        jPanel2.add(txtTelefono, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 160, 160, -1));

        jLabel18.setFont(new java.awt.Font("Roboto", 1, 14)); // NOI18N
        jLabel18.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel18.setText("Correo:");
        jPanel2.add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 190, 110, 20));

        txtCorreo.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        txtCorreo.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jPanel2.add(txtCorreo, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 190, 160, -1));

        jLabel30.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        jLabel30.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel30.setText("%");
        jPanel2.add(jLabel30, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 250, 30, 20));

        jLabel31.setFont(new java.awt.Font("Roboto", 1, 14)); // NOI18N
        jLabel31.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel31.setText("Facturas:");
        jPanel2.add(jLabel31, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 220, 130, 20));

        jLabel32.setFont(new java.awt.Font("Roboto", 1, 14)); // NOI18N
        jLabel32.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel32.setText("Teléfono 2:");
        jPanel2.add(jLabel32, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 160, 110, 20));

        spinUnidadesFActuras.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spinUnidadesFActurasStateChanged(evt);
            }
        });
        jPanel2.add(spinUnidadesFActuras, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 220, 100, -1));

        txtIva.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        txtIva.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtIva.setText("0");
        jPanel2.add(txtIva, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 250, 60, 28));

        jLabel33.setFont(new java.awt.Font("Roboto", 1, 14)); // NOI18N
        jLabel33.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel33.setText("Iva definido:");
        jPanel2.add(jLabel33, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 250, 110, 20));

        txtAutorizacion.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        txtAutorizacion.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        jPanel2.add(txtAutorizacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 190, 160, -1));

        txtApellido.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        txtApellido.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        jPanel2.add(txtApellido, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 70, 160, -1));

        getContentPane().add(jPanel2);
        jPanel2.setBounds(0, 0, 620, 360);

        setSize(new java.awt.Dimension(634, 397));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btn_guardarPersonaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_guardarPersonaMouseClicked
        guardar();
    }//GEN-LAST:event_btn_guardarPersonaMouseClicked

    private void btn_cancelarPersonaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_cancelarPersonaMouseClicked
        // TODO add your handling code here:
//        Frm_DatosEmpresa nuevo = new Frm_DatosEmpresa();
//        nuevo.setVisible(true);
//        this.dispose();
        limpiarDatos();
    }//GEN-LAST:event_btn_cancelarPersonaMouseClicked

    private void btn_irFactura1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_irFactura1MouseClicked
        // TODO add your handling code here:
        Frm_vistaFactura nuevaFactura = new Frm_vistaFactura();
        nuevaFactura.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btn_irFactura1MouseClicked

    private void txtNombreEMPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNombreEMPActionPerformed

    }//GEN-LAST:event_txtNombreEMPActionPerformed

    private void btn_actualizarPersonaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_actualizarPersonaMouseClicked
        actualizar();
    }//GEN-LAST:event_btn_actualizarPersonaMouseClicked

    private void spinUnidadesFActurasStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spinUnidadesFActurasStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spinUnidadesFActurasStateChanged

    /**
     * @param args the command line arguments
     */
    //en este método guardaremos toda la información , siempre y cuando esté verificada y no contenga espacios en blanco
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Frm_DatosEmpresa.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Frm_DatosEmpresa.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Frm_DatosEmpresa.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Frm_DatosEmpresa.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Frm_DatosEmpresa().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel btn_actualizarPersona;
    private javax.swing.JLabel btn_cancelarPersona;
    private javax.swing.JLabel btn_guardarPersona;
    private javax.swing.JLabel btn_irFactura1;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSpinner spinUnidadesFActuras;
    public static javax.swing.JTextField txtApellido;
    public static javax.swing.JTextField txtAutorizacion;
    public static javax.swing.JTextField txtCorreo;
    public static javax.swing.JTextField txtCorreoEMP;
    public static javax.swing.JTextField txtDireccion;
    public static javax.swing.JTextField txtDireccionEMP;
    private com.toedter.calendar.JDateChooser txtFechaAutorizacion;
    private com.toedter.calendar.JDateChooser txtFechaCaducidad;
    public static javax.swing.JTextField txtIdentificacion;
    public static javax.swing.JTextField txtIva;
    public static javax.swing.JTextField txtNombre;
    public static javax.swing.JTextField txtNombreEMP;
    public static javax.swing.JTextField txtTelefono;
    public static javax.swing.JTextField txtTelefonoEMP;
    public static javax.swing.JTextField txtTelefonoEMP2;
    // End of variables declaration//GEN-END:variables
}
