/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import controlador.daos.ProductoDao;
import javax.swing.JOptionPane;
import modelo.DetalleFactura;
import modelo.Producto;

/**
 *
 * @author oMAR MALDONADO
 */
public class Frm_agregarProductos extends javax.swing.JFrame {

    private Frm_Productos frmProductos;
    /**
     * Variables globales
     */
    private ProductoDao productodao = new ProductoDao();
    private DetalleFactura detalleFactura = new DetalleFactura();
    private int dat;

    /**
     * Constructor 1 de la vista No tiene botón de eliminar o modificar
     */
    public Frm_agregarProductos() {

        initComponents();
        txt_agreCodigoProducto.setText(String.valueOf(productodao.listar().tamanio() + 1));
        limpiar();
        btn_modificar.setVisible(false);
        btn_modificar.setEnabled(false);
        jPanelModificar.setVisible(false);
        jPanelModificar.setEnabled(false);
        btn_eliminar.setVisible(false);
        btn_eliminar.setEnabled(false);
        jPanelEliminar.setVisible(false);
        jPanelEliminar.setEnabled(false);

    }

    public Frm_agregarProductos(Object object, boolean b, Frm_Productos frm) {

        initComponents();
        this.frmProductos = frm;
        btn_modificar.setVisible(false);
        btn_modificar.setEnabled(false);
        jPanelModificar.setVisible(false);
        jPanelModificar.setEnabled(false);
        btn_eliminar.setVisible(false);
        btn_eliminar.setEnabled(false);
        jPanelEliminar.setVisible(false);
        jPanelEliminar.setEnabled(false);

    }

    /**
     * Constructor 2 de la vista Muestra el botón modificar
     */
    public Frm_agregarProductos(ProductoDao p, int fila) {
        initComponents();
        asignarValores(p, fila);
        btn_eliminar.setVisible(false);
        btn_eliminar.setEnabled(false);
        jPanelEliminar.setVisible(false);
        jPanelEliminar.setEnabled(false);

    }

    /**
     * Constructor 3 de la vista Muestra el botón modificar
     */
    public Frm_agregarProductos(ProductoDao p, int fila, Boolean eliminar) {
        initComponents();
        asignarValores(p, fila);
        btn_modificar.setVisible(false);
        btn_modificar.setEnabled(false);
        jPanelModificar.setVisible(false);
        jPanelModificar.setEnabled(false);

    }

    /**
     * Cuando se va a modificr o eliminar un dato este método permite que se
     * vean los datos
     */
    public void asignarValores(ProductoDao p, int fila) {
        productodao = p;

        txt_agreCodigoProducto.setText(((Producto) productodao.soloVisibles().consultarDatoPosicion(fila)).getId().toString());
        txt_agreDescripcionProducto.setText(((Producto) productodao.soloVisibles().consultarDatoPosicion(fila)).getDescripcion());
        txt_agreNombreProducto.setText(((Producto) productodao.soloVisibles().consultarDatoPosicion(fila)).getNombre());
        txt_agrePrecioProducto.setText(((Producto) productodao.soloVisibles().consultarDatoPosicion(fila)).getPrecioIngresado().toString());
        rbtn_IncluyeIva.setSelected(((Producto) productodao.soloVisibles().consultarDatoPosicion(fila)).getIncluyeIva());
        rbtn_LibreDeImpuestos.setSelected(((Producto) productodao.soloVisibles().consultarDatoPosicion(fila)).getProductolibredeImpuestos());

    }

    /**
     * Permite asignar un valor a producto dato
     */
    public void setProducto(ProductoDao p) {
        productodao = p;
    }

    /**
     * Comprueba que estén todos los campos llenos
     */
    private boolean validar() {
        return (txt_agreCodigoProducto.getText().trim().length() > 0
                && txt_agreNombreProducto.getText().trim().length() > 0
                && txt_agrePrecioProducto.getText().trim().length() > 0);
    }

    /**
     * Borra todos los campos llenos y asigna el siguiente valor para el c´digo
     */
    private void limpiar() {
        String enumerador = String.valueOf(productodao.getLista().tamanio() + 1);
        System.out.println(enumerador);
        txt_agreCodigoProducto.setText(enumerador);
        txt_agreNombreProducto.setText("");
        txt_agreDescripcionProducto.setText("");
        rbtn_LibreDeImpuestos.setSelected(false);
        rbtn_IncluyeIva.setSelected(false);
        txt_agrePrecioProducto.setText("");

    }

    /**
     * Actualiza los datos del DaoProducto
     */

    private void actualizar(Boolean eliminar) {
        try {

            if (validar()) {
                System.out.println("Entra al if");
                productodao.getProducto().setNombre(txt_agreNombreProducto.getText());
                productodao.getProducto().setDescripcion(txt_agreDescripcionProducto.getText());
                productodao.getProducto().setPrecioIngresado(Double.parseDouble(txt_agrePrecioProducto.getText().trim()));
                productodao.getProducto().setIncluyeIva(rbtn_IncluyeIva.isSelected());
                productodao.getProducto().setProductolibredeImpuestos(rbtn_LibreDeImpuestos.isSelected());
                productodao.getProducto().setPrecioconiva(Double.parseDouble(txt_agrePrecioProducto.getText().trim()), detalleFactura.getIva());
                productodao.getProducto().setPreciosiniva(Double.parseDouble(txt_agrePrecioProducto.getText().trim()), detalleFactura.getIva());
                System.out.println("Entra aqui a actualizar");
                System.out.println(productodao.getProducto().getVisible());
                productodao.getProducto().setVisible(eliminar);
                System.out.println(productodao.getProducto().getVisible());

                if (productodao.modificar()) {
                    JOptionPane.showMessageDialog(null, "Se ha registrado correctamente", "Ok", JOptionPane.WARNING_MESSAGE);
                    limpiar();

                } else {
                    JOptionPane.showMessageDialog(null, "Hubo un error", "Error", JOptionPane.ERROR_MESSAGE);

                }

            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Valores inválidos o incompletos", "Error", JOptionPane.ERROR_MESSAGE);

        }

    }

    /**
     * Guarda los datos del DaoProducto
     */

    private void guardar() {
        try {

            if (validar()) {

                //Resto de datos
                productodao.getProducto().setDescripcion(txt_agreDescripcionProducto.getText().trim());
                productodao.getProducto().setId(Long.valueOf(txt_agreCodigoProducto.getText().trim()));
                productodao.getProducto().setNombre(txt_agreNombreProducto.getText().trim());
                productodao.getProducto().setProductolibredeImpuestos(rbtn_LibreDeImpuestos.isSelected());
                productodao.getProducto().setIncluyeIva(rbtn_IncluyeIva.isSelected());
                productodao.getProducto().setPrecioIngresado(Double.parseDouble(txt_agrePrecioProducto.getText().trim()));

                productodao.getProducto().setPrecioconiva(Double.parseDouble(txt_agrePrecioProducto.getText().trim()), detalleFactura.getIva());
                productodao.getProducto().setPreciosiniva(Double.parseDouble(txt_agrePrecioProducto.getText().trim()), detalleFactura.getIva());

                System.out.println(productodao.getProducto().getIncluyeIva());

                if (productodao.guardarProducto()) {
                    JOptionPane.showMessageDialog(null, "Guardado con ëxito", "Ok", JOptionPane.INFORMATION_MESSAGE);
                    txt_agreCodigoProducto.setText(String.valueOf(productodao.listar().tamanio()));
                    limpiar();
                } else {
                    JOptionPane.showMessageDialog(null, "No se puede guardar", "Error", JOptionPane.ERROR_MESSAGE);

                }
            } else {
                JOptionPane.showMessageDialog(null, "No se puede guardar", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Valores inválidos o incompletos", "Error", JOptionPane.ERROR_MESSAGE);

        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel5 = new javax.swing.JLabel();
        txt_agreCodigoProducto = new javax.swing.JTextField();
        txt_agreNombreProducto = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        txt_agrePrecioProducto = new javax.swing.JTextField();
        jPanelEliminar = new javax.swing.JPanel();
        btn_eliminar = new javax.swing.JLabel();
        jPanelModificar = new javax.swing.JPanel();
        btn_modificar = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        btn_guardarProducto = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        btn_cancelarProducto = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        btn_irFactura = new javax.swing.JLabel();
        txt_agreDescripcionProducto = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        rbtn_LibreDeImpuestos = new javax.swing.JRadioButton();
        rbtn_IncluyeIva = new javax.swing.JRadioButton();
        jLabel11 = new javax.swing.JLabel();

        jLabel2.setFont(new java.awt.Font("Roboto", 1, 18)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("PRODUCTOS");

        jLabel3.setFont(new java.awt.Font("Roboto", 1, 18)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("PRODUCTOS");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel4.setFont(new java.awt.Font("Roboto", 1, 18)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("PRODUCTOS");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 10, 110, 20));

        jLabel1.setFont(new java.awt.Font("Roboto", 1, 14)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel1.setText("Código:");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, 70, 20));
        jPanel1.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, 280, 10));

        jLabel5.setFont(new java.awt.Font("Roboto", 1, 18)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("AGREGAR PRODUCTOS");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, 270, 20));

        txt_agreCodigoProducto.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        txt_agreCodigoProducto.setFocusable(false);
        jPanel1.add(txt_agreCodigoProducto, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 50, 150, -1));

        txt_agreNombreProducto.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        jPanel1.add(txt_agreNombreProducto, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 80, 150, -1));

        jLabel7.setFont(new java.awt.Font("Roboto", 1, 14)); // NOI18N
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel7.setText("Nombre:");
        jPanel1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 80, 110, 20));

        jLabel9.setFont(new java.awt.Font("Roboto", 1, 14)); // NOI18N
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel9.setText("Precio:");
        jPanel1.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 170, 110, 20));

        txt_agrePrecioProducto.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        txt_agrePrecioProducto.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jPanel1.add(txt_agrePrecioProducto, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 170, 150, -1));

        jPanelEliminar.setBackground(new java.awt.Color(0, 102, 255));
        jPanelEliminar.setForeground(new java.awt.Color(0, 51, 255));

        btn_eliminar.setFont(new java.awt.Font("Roboto", 1, 11)); // NOI18N
        btn_eliminar.setForeground(new java.awt.Color(255, 255, 255));
        btn_eliminar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btn_eliminar.setText("ELIMINAR");
        btn_eliminar.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btn_eliminar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_eliminarMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanelEliminarLayout = new javax.swing.GroupLayout(jPanelEliminar);
        jPanelEliminar.setLayout(jPanelEliminarLayout);
        jPanelEliminarLayout.setHorizontalGroup(
            jPanelEliminarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_eliminar, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
        );
        jPanelEliminarLayout.setVerticalGroup(
            jPanelEliminarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_eliminar, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        jPanel1.add(jPanelEliminar, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 270, 120, 30));

        jPanelModificar.setBackground(new java.awt.Color(0, 102, 255));
        jPanelModificar.setForeground(new java.awt.Color(0, 51, 255));

        btn_modificar.setFont(new java.awt.Font("Roboto", 1, 11)); // NOI18N
        btn_modificar.setForeground(new java.awt.Color(255, 255, 255));
        btn_modificar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btn_modificar.setText("MODIFICAR");
        btn_modificar.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btn_modificar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_modificarMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanelModificarLayout = new javax.swing.GroupLayout(jPanelModificar);
        jPanelModificar.setLayout(jPanelModificarLayout);
        jPanelModificarLayout.setHorizontalGroup(
            jPanelModificarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_modificar, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
        );
        jPanelModificarLayout.setVerticalGroup(
            jPanelModificarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_modificar, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        jPanel1.add(jPanelModificar, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 270, 120, 30));

        jPanel3.setBackground(new java.awt.Color(0, 102, 255));
        jPanel3.setForeground(new java.awt.Color(0, 51, 255));

        btn_guardarProducto.setFont(new java.awt.Font("Roboto", 1, 11)); // NOI18N
        btn_guardarProducto.setForeground(new java.awt.Color(255, 255, 255));
        btn_guardarProducto.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btn_guardarProducto.setText("GUARDAR");
        btn_guardarProducto.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btn_guardarProducto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_guardarProductoMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_guardarProducto, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_guardarProducto, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 310, 120, 30));

        jPanel5.setBackground(new java.awt.Color(0, 102, 255));
        jPanel5.setForeground(new java.awt.Color(0, 51, 255));

        btn_cancelarProducto.setFont(new java.awt.Font("Roboto", 1, 11)); // NOI18N
        btn_cancelarProducto.setForeground(new java.awt.Color(255, 255, 255));
        btn_cancelarProducto.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btn_cancelarProducto.setText("ATRAS");
        btn_cancelarProducto.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btn_cancelarProducto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_cancelarProductoMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_cancelarProducto, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_cancelarProducto, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 350, -1, -1));

        jPanel6.setBackground(new java.awt.Color(0, 102, 255));
        jPanel6.setForeground(new java.awt.Color(0, 51, 255));

        btn_irFactura.setFont(new java.awt.Font("Roboto", 1, 11)); // NOI18N
        btn_irFactura.setForeground(new java.awt.Color(255, 255, 255));
        btn_irFactura.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btn_irFactura.setText("FACTURAR");
        btn_irFactura.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btn_irFactura.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_irFacturaMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_irFactura, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_irFactura, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 390, -1, -1));

        txt_agreDescripcionProducto.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        txt_agreDescripcionProducto.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jPanel1.add(txt_agreDescripcionProducto, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 110, 150, 50));

        jLabel10.setFont(new java.awt.Font("Roboto", 1, 14)); // NOI18N
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel10.setText("Descripción:");
        jPanel1.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 110, 110, 20));

        rbtn_LibreDeImpuestos.setFont(new java.awt.Font("Roboto", 1, 12)); // NOI18N
        rbtn_LibreDeImpuestos.setText("Libre de Impuestos");
        rbtn_LibreDeImpuestos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtn_LibreDeImpuestosActionPerformed(evt);
            }
        });
        jPanel1.add(rbtn_LibreDeImpuestos, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 240, -1, -1));

        rbtn_IncluyeIva.setFont(new java.awt.Font("Roboto", 1, 12)); // NOI18N
        rbtn_IncluyeIva.setText("Incluye tiene iva");
        rbtn_IncluyeIva.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtn_IncluyeIvaActionPerformed(evt);
            }
        });
        jPanel1.add(rbtn_IncluyeIva, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 210, -1, -1));

        jLabel11.setFont(new java.awt.Font("Roboto", 1, 14)); // NOI18N
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel11.setText("Precio:");
        jPanel1.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 170, 110, 20));

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 300, 440);

        setSize(new java.awt.Dimension(316, 478));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Se asigna los métodos según cprresponda e los disintos botones
     */
    private void btn_irFacturaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_irFacturaMouseClicked
        // TODO add your handling code here:
        Frm_vistaFactura nuevaFactura = new Frm_vistaFactura();
        nuevaFactura.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btn_irFacturaMouseClicked

    private void btn_cancelarProductoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_cancelarProductoMouseClicked
        // TODO add your handling code here:
        Frm_Productos nuevoProducto = new Frm_Productos();
        nuevoProducto.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btn_cancelarProductoMouseClicked

    private void btn_guardarProductoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_guardarProductoMouseClicked
        // TODO add your handling code here:

        guardar();
        limpiar();
    }//GEN-LAST:event_btn_guardarProductoMouseClicked

    private void rbtn_LibreDeImpuestosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtn_LibreDeImpuestosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rbtn_LibreDeImpuestosActionPerformed

    private void rbtn_IncluyeIvaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtn_IncluyeIvaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rbtn_IncluyeIvaActionPerformed

    private void btn_modificarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_modificarMouseClicked
        // TODO add your handling code here:
        actualizar(true);
        Frm_Productos nuevoProducto = new Frm_Productos();
        nuevoProducto.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btn_modificarMouseClicked

    private void btn_eliminarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_eliminarMouseClicked
        // TODO add your handling code here:
        actualizar(false);
        Frm_Productos nuevoProducto = new Frm_Productos();
        nuevoProducto.setVisible(true);
        this.dispose();

    }//GEN-LAST:event_btn_eliminarMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Frm_agregarProductos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Frm_agregarProductos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Frm_agregarProductos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Frm_agregarProductos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Frm_agregarProductos().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel btn_cancelarProducto;
    private javax.swing.JLabel btn_eliminar;
    private javax.swing.JLabel btn_guardarProducto;
    private javax.swing.JLabel btn_irFactura;
    private javax.swing.JLabel btn_modificar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanelEliminar;
    private javax.swing.JPanel jPanelModificar;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JRadioButton rbtn_IncluyeIva;
    private javax.swing.JRadioButton rbtn_LibreDeImpuestos;
    private javax.swing.JTextField txt_agreCodigoProducto;
    private javax.swing.JTextField txt_agreDescripcionProducto;
    private javax.swing.JTextField txt_agreNombreProducto;
    private javax.swing.JTextField txt_agrePrecioProducto;
    // End of variables declaration//GEN-END:variables
}
