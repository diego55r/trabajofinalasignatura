package vista;

import controlador.CtrolDetalleFactura;
import controlador.CtrolFactura;
import controlador.daos.EmpresaDao;
import controlador.daos.FacturaDao;
import controlador.daos.PersonaDao;
import controlador.daos.ProductoDao;
import java.awt.Color;
import java.text.DecimalFormat;
import java.util.Calendar;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import lista.controlador.Lista;
import vista.modelo.tablaProductoFactura;

public class Frm_vistaFactura extends javax.swing.JFrame {

    private FacturaDao facturaDao = new FacturaDao();
    private CtrolDetalleFactura ctrdetalle = new CtrolDetalleFactura();
    private CtrolFactura factura = new CtrolFactura();
    private PersonaDao personaDao = new PersonaDao();
    private EmpresaDao empresaDao = new EmpresaDao();
    private ProductoDao productoDao = new ProductoDao();
    private tablaProductoFactura mtc = new tablaProductoFactura();
    private Lista aux = new Lista();
    private DecimalFormat df = new DecimalFormat("###.##");

    public Frm_vistaFactura() {
        initComponents();
        txtIva.setText(empresaDao.getEmpresa().getIva().toString());

        cargarTabla();
        cargarDatosProforma();

    }
/**
 * carga la tabla productos carrito 
 */
    private void cargarTabla() {
        Lista aux = new Lista();
        mtc.setLista(aux);
        tablaProductos.setModel(mtc);
        tablaProductos.updateUI();
    }
    
/**
 * carga los datos de la proforma cargada en el archivo de proformas seleccionadas
 */
    private void cargarDatosProforma() {
        try {
            if (factura.listar().consultarDatoPosicion(factura.listar().tamanio()-1) != null) {
                facturaDao.setFactura(factura.listar().consultarDatoPosicion(factura.listar().tamanio() - 1));
                personaDao.setPersona(facturaDao.getFactura().getCliente());
                ctrdetalle.setDf(facturaDao.getFactura().getDetalleFactura());
                ctrdetalle.setListaProductos(facturaDao.getFactura().getDetalleFactura().getProductos());
                txtCedula.setText(facturaDao.getFactura().getCliente().getIdentificador());
                txtDireccion.setText(facturaDao.getFactura().getCliente().getDireccion());
                txtEmail.setText(facturaDao.getFactura().getCliente().getCorreo());
                txtCelular.setText(facturaDao.getFactura().getCliente().getTelefono());
                mtc.setLista(facturaDao.getFactura().getDetalleFactura().getProductos());
                tablaProductos.setModel(mtc);
                tablaProductos.updateUI();
                txtSubTotal.setText(df.format(facturaDao.getFactura().getDetalleFactura().getSubTotal()));
                txtTotal.setText(df.format(facturaDao.getFactura().getDetalleFactura().getTotal()));
                txtSubTotalIva.setText(df.format(facturaDao.getFactura().getDetalleFactura().getIvaTotal()));
            }
        } catch (Exception e) {

        }
    }
/**
 * valida los campos necesario  para la facturacion
 */
    private boolean validar() {
        Boolean val1 = false;
        Boolean val2 = false;
        Boolean val3 = false;
        if (tipoPago() != null) {
            facturaDao.getFactura().setFormaPago(tipoPago());
            val1 = true;
        } else {
            JOptionPane.showMessageDialog(null, "Debe seleccionar un método de pago", "Método de pago requerido", JOptionPane.INFORMATION_MESSAGE);
        }
        if (personaDao.getPersona().getNombre() != null) {
            val2 = true;
        } else {
            JOptionPane.showMessageDialog(null, "Debe ingresar el cliente", "Cliente requerido", JOptionPane.INFORMATION_MESSAGE);
        }

        if (ctrdetalle.getDf() != null) {

            val3 = true;
        } else {
            JOptionPane.showMessageDialog(null, "Debe ingresar al menos un producto", "Prooducto Requerido", JOptionPane.INFORMATION_MESSAGE);
        }
        return val1 && val2;
    }

    private void limpiar() {
        txtCedula.setText("");
        txtCelular.setText("");
        txtDireccion.setText("");
        txtEmail.setText("");
        txtSubTotal.setText("");
        txtSubTotalIva.setText("");
        txtTotal.setText("");
        txtvalorTotalProducto.setText("");
        txtValorUnitProducto.setText("");
        radioCredito.setSelected(false);
        radioDepositoTransferencia.setSelected(false);
        radioEfectivo.setSelected(false);
        radioOtros.setSelected(false);
        limpiarProducto();
        limpiarDatos();
        cargarTabla();

    }

    private void limpiarDatos() {
        facturaDao.setFactura(null);
        ctrdetalle = new CtrolDetalleFactura();
        personaDao.setPersona(null);
        productoDao.setProducto(null);
    }

    private void limpiarProducto() {
        txtBuscaProduto.setText("");

        txtvalorTotalProducto.setText("");
        txtValorUnitProducto.setText("");
        spinUnidadesProducto.setValue(0);
        productoDao.setProducto(null);
    }

    private void selecionarUnidadesProducto() {
        try {
            spinUnidadesProducto.commitEdit();
        } catch (java.text.ParseException e) {
        }
        int unidades = (Integer) spinUnidadesProducto.getValue();
        System.out.println("unidades + " + unidades);
        try {
            if (unidades > 0) {
                Double valorProductoT = productoDao.getProducto().getPreciosiniva() * unidades;
                txtvalorTotalProducto.setText(df.format(valorProductoT) + "");
            }
        } catch (Exception e) {
        }
    }

    private void llenarTabla() {

        try {
            spinUnidadesProducto.commitEdit();
        } catch (java.text.ParseException e) {
        }
        int unidades = (Integer) spinUnidadesProducto.getValue();
        if (unidades > 0) {
            try {
                if (productoDao.getProducto().getNombre() != null) {
                    productoDao.getProducto().setUnidades(unidades);
                    ctrdetalle.setProducto(productoDao.getProducto());
                    ctrdetalle.getDf().setIva(empresaDao.getEmpresa().getIva());
                    ctrdetalle.getListaProductos().insertarNodo(ctrdetalle.getProducto());
                    mtc.setLista(ctrdetalle.getListaProductos());
                    ctrdetalle.getDf().setProductos(mtc.getLista());
                    tablaProductos.setModel(mtc);
                    tablaProductos.updateUI();
                    txtIva.setText("");

                    llenarPrecios();
                    limpiarProducto();
                } else {
                    JOptionPane.showMessageDialog(null, "Ingrese el producto correctamente", "Error", JOptionPane.ERROR_MESSAGE);
                    System.out.println("-----asasasa");
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Ingrese el producto correctamente", "Error", JOptionPane.ERROR_MESSAGE);
                e.printStackTrace();
            }
        } else {
            JOptionPane.showMessageDialog(null, "Coloque las unidades correctante", "Error", JOptionPane.ERROR_MESSAGE);
        }

    }

    private void llenarPrecios() {
        try {
            Double SubTotal = 0.0;
            Double IvaTotal = 0.0;
            Double Total = 0.0;
            DecimalFormat df = new DecimalFormat("###.##");

            for (int i = 0; i < ctrdetalle.getListaProductos().tamanio(); i++) {
                SubTotal += ctrdetalle.getListaProductos().consultarDatoPosicion(i).getPreciosiniva() * ctrdetalle.getListaProductos().consultarDatoPosicion(i).getUnidades();
                IvaTotal += (ctrdetalle.getListaProductos().consultarDatoPosicion(i).getPrecioconiva() - ctrdetalle.getListaProductos().consultarDatoPosicion(i).getPreciosiniva()) * ctrdetalle.getListaProductos().consultarDatoPosicion(i).getUnidades();
                Total += ctrdetalle.getListaProductos().consultarDatoPosicion(i).getPrecioconiva() * ctrdetalle.getListaProductos().consultarDatoPosicion(i).getUnidades();
                ctrdetalle.getDf().setSubTotal(SubTotal);
                ctrdetalle.getDf().setIvaTotal(IvaTotal);
                ctrdetalle.getDf().setTotal(Total);
                txtSubTotal.setText(df.format(SubTotal));
                txtSubTotalIva.setText(df.format(IvaTotal));
                txtTotal.setText(df.format(Total));
            }
        } catch (Exception e) {
        }

    }

    private String tipoPago() {
        if (radioEfectivo.isSelected()) {
            return "Efectivo";
        } else if (radioCredito.isSelected()) {
            return "Crédito/Débito";
        } else if (radioDepositoTransferencia.isSelected()) {
            return "Depósito/Transferencia";
        } else if (radioOtros.isSelected()) {
            return "Otro";
        }
        return null;
    }

    private void guardar(Boolean aprobada) {
        int opcion;

        if (validar()) {
            if (aprobada) {
                opcion = JOptionPane.showConfirmDialog(null, "¿Está seguro de realizar la compra?");
            } else {
                opcion = JOptionPane.showConfirmDialog(null, "Se registrá como proforma");
            }
            if (opcion == 0) {
                ctrdetalle.getDf().setIva(empresaDao.getEmpresa().getIva());
                facturaDao.getFactura().setFacturaAprobada(aprobada);
                facturaDao.getFactura().setDetalleFactura(ctrdetalle.getDf());
                facturaDao.getFactura().setEmpresa(empresaDao.listar().consultarDatoPosicion(empresaDao.listar().tamanio() - 1));
                Calendar fecha = Calendar.getInstance();
                facturaDao.getFactura().setFecha(fecha);
                facturaDao.getFactura().setCliente(personaDao.getPersona());
                try {
                    facturaDao.setEmpresa(empresaDao.listar().consultarDatoPosicion(0));
                    if (facturaDao.guardar(aprobada)) {
                        JOptionPane.showMessageDialog(null, "Guardado con exito", "OK", JOptionPane.INFORMATION_MESSAGE);
                        facturaDao.imprimirDatosFactura(aprobada);
                        limpiar();
                        factura.guardar(null);
                    } else {
                        JOptionPane.showMessageDialog(null, "No se puede guardar", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, "No se ha ingresado datos de la empresa", "Error", JOptionPane.ERROR_MESSAGE);
                    JOptionPane.showMessageDialog(null, "No se puede guardar", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        } else {
            JOptionPane.showMessageDialog(null, "No se puede guardar", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void guardarDatosDetalle() {

    }

    private void buscarCliente() {

        personaDao.setPersona(personaDao.buscarPorCedula(personaDao.listar(), txtCedula.getText().trim()));
        if (personaDao.getPersona().getIdentificador() != null) {

            txtDireccion.setText(personaDao.getPersona().getDireccion());
            txtEmail.setText(personaDao.getPersona().getCorreo());
            txtCelular.setText(personaDao.getPersona().getTelefono());
            facturaDao.getFactura().setCliente(personaDao.getPersona());
        } else {
            JOptionPane.showMessageDialog(null, "Aún no se ha registrado el cliente", "Información", JOptionPane.INFORMATION_MESSAGE);
        }

    }

    private void buscarProducto() {
        try {
            productoDao.setProducto(null);
            productoDao.setProducto(productoDao.buscarPorID(productoDao.listar(), txtBuscaProduto.getText().trim()));
            if (productoDao.getProducto().getId() == null) {
                productoDao.setProducto(productoDao.buscarString(txtBuscaProduto.getText().trim()).consultarDatoPosicion(0));
            }
            if (productoDao.getProducto().getId() != null) {
                txtBuscaProduto.setText(productoDao.getProducto().getId() + " - " + productoDao.getProducto().getNombre());
                spinUnidadesProducto.setValue(1);
                Double auxSinIva = productoDao.getProducto().getPreciosiniva();
                txtValorUnitProducto.setText(df.format(auxSinIva) + "");
                txtvalorTotalProducto.setText(df.format(auxSinIva) + "");
                if (productoDao.getProducto().getIncluyeIva()) {

                    txtIva.setText("" + empresaDao.getEmpresa().getIva());
                } else {
                    txtIva.setText("0%");
                }

            }
            if (productoDao.getProducto().getId() == null) {
                JOptionPane.showMessageDialog(null, "No se ha encontrado el producto", "Información", JOptionPane.INFORMATION_MESSAGE);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "No se ha encontrado el producto", "Información", JOptionPane.INFORMATION_MESSAGE);
            e.printStackTrace();
        }

    }

    private void modificarIva() {
        //validar si es admin
        String iva = "";
        int opcion = -1;
        try {
            iva = JOptionPane.showInputDialog("Ingrese el iva requerido: ");
            Double IVA = 0.0;
            if (iva != "") {
                IVA = Double.parseDouble(iva);
            }
            if (IVA >= 0.0 && IVA <= 100) {   
                opcion = JOptionPane.showConfirmDialog(null, "¿Está seguro de cambiar el iva al " + iva + "%?");
            }
            if (opcion == 0) {
                empresaDao.getEmpresa().setIva(IVA);
                if (empresaDao.modificar(empresaDao.getEmpresa(), empresaDao.listar().tamanio() - 1)) {
                    txtIva.setText(empresaDao.getEmpresa().getIva() + "%");
                } else {

                }
            }
        } catch (Exception e) {
            if (opcion == 0 && iva != null) {
                JOptionPane.showMessageDialog(null, "Debe ingresar un valor numérico", "Error", JOptionPane.ERROR_MESSAGE);

            }
        }

    }

    private void editarCarrito() {
        int opcion = JOptionPane.showConfirmDialog(null, "Ha seleccionado un producto desea eliminarlo de la compra?");
        if (opcion == 0) {
            int podProducto = tablaProductos.getSelectedRow();
            CtrolDetalleFactura ctrDetalleAux = new CtrolDetalleFactura();
            for (int i = 0; i < ctrdetalle.getListaProductos().tamanio(); i++) {
                if (i != podProducto) {
                    ctrDetalleAux.getListaProductos().insertarNodo(ctrdetalle.getListaProductos().consultarDatoPosicion(i));
                }
            }
            ctrdetalle = ctrDetalleAux;

            mtc.setLista(ctrdetalle.getListaProductos());
            ctrdetalle.getDf().setProductos(mtc.getLista());
            tablaProductos.setModel(mtc);
            tablaProductos.updateUI();

            llenarPrecios();
            limpiarProducto();
        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel9 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable3 = new javax.swing.JTable();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTable4 = new javax.swing.JTable();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();
        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        buttonGroup3 = new javax.swing.ButtonGroup();
        buttonGroup4 = new javax.swing.ButtonGroup();
        buttonGroup5 = new javax.swing.ButtonGroup();
        jPanel2 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        panel_cerrarSesion = new javax.swing.JPanel();
        btn_cerrarSesion = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        btn_consulClientes = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        btn_consulProductos = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        btn_agregarClientes = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        btn_agregarProductos = new javax.swing.JLabel();
        btn_consultarFactuas = new javax.swing.JLabel();
        btn_consultarProformas = new javax.swing.JLabel();
        btn_verDatosEmpresa = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaProductos = new javax.swing.JTable();
        jLabel6 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jPanel11 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTable5 = new javax.swing.JTable();
        jLabel18 = new javax.swing.JLabel();
        jPanel12 = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        jTable6 = new javax.swing.JTable();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        txtValorUnitProducto = new javax.swing.JTextField();
        txtvalorTotalProducto = new javax.swing.JTextField();
        jLabel24 = new javax.swing.JLabel();
        agregarProd = new javax.swing.JButton();
        txtBuscaProduto = new javax.swing.JTextField();
        buscarProducto = new javax.swing.JButton();
        spinUnidadesProducto = new javax.swing.JSpinner();
        jLabel4 = new javax.swing.JLabel();
        txtIva = new javax.swing.JTextField();
        jLabel25 = new javax.swing.JLabel();
        txtSubTotalIva = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        txtSubTotal = new javax.swing.JTextField();
        jLabel28 = new javax.swing.JLabel();
        txtTotal = new javax.swing.JTextField();
        jLabel30 = new javax.swing.JLabel();
        radioOtros = new javax.swing.JRadioButton();
        radioEfectivo = new javax.swing.JRadioButton();
        radioCredito = new javax.swing.JRadioButton();
        radioDepositoTransferencia = new javax.swing.JRadioButton();
        Proformar = new javax.swing.JButton();
        Facturar = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        txtCelular = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtEmail = new javax.swing.JTextField();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txtDireccion = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtCedula = new javax.swing.JTextField();
        btBuscar = new javax.swing.JButton();

        jPanel9.setBackground(new java.awt.Color(255, 255, 255));
        jPanel9.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel9.setLayout(null);

        jTable3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane3.setViewportView(jTable3);

        jPanel9.add(jScrollPane3);
        jScrollPane3.setBounds(10, 150, 570, 80);

        jLabel13.setText("Detalle de la factura (Moneda dólares americanos $$)");
        jPanel9.add(jLabel13);
        jLabel13.setBounds(10, 10, 270, 14);

        jLabel14.setText("Datos del Comprador");
        jPanel9.add(jLabel14);
        jLabel14.setBounds(10, 110, 101, 14);

        jPanel10.setBackground(new java.awt.Color(255, 255, 255));
        jPanel10.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel10.setLayout(null);

        jTable4.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane4.setViewportView(jTable4);

        jPanel10.add(jScrollPane4);
        jScrollPane4.setBounds(10, 150, 570, 80);

        jLabel15.setText("Detalle de la factura (Moneda dólares americanos $$)");
        jPanel10.add(jLabel15);
        jLabel15.setBounds(10, 10, 270, 14);

        jLabel16.setText("Datos del Comprador");
        jPanel10.add(jLabel16);
        jLabel16.setBounds(10, 110, 101, 14);

        jPanel9.add(jPanel10);
        jPanel10.setBounds(0, 0, 0, 0);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(51, 0, 102));
        jPanel1.setLayout(null);

        panel_cerrarSesion.setBackground(new java.awt.Color(51, 0, 102));

        btn_cerrarSesion.setBackground(new java.awt.Color(51, 0, 102));
        btn_cerrarSesion.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        btn_cerrarSesion.setForeground(new java.awt.Color(255, 255, 255));
        btn_cerrarSesion.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btn_cerrarSesion.setText("Cerrar Sesión");
        btn_cerrarSesion.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btn_cerrarSesion.setName(""); // NOI18N
        btn_cerrarSesion.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_cerrarSesionMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btn_cerrarSesionMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btn_cerrarSesionMouseExited(evt);
            }
        });

        javax.swing.GroupLayout panel_cerrarSesionLayout = new javax.swing.GroupLayout(panel_cerrarSesion);
        panel_cerrarSesion.setLayout(panel_cerrarSesionLayout);
        panel_cerrarSesionLayout.setHorizontalGroup(
            panel_cerrarSesionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_cerrarSesion, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
        );
        panel_cerrarSesionLayout.setVerticalGroup(
            panel_cerrarSesionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_cerrarSesion, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        jPanel1.add(panel_cerrarSesion);
        panel_cerrarSesion.setBounds(0, 440, 170, 30);

        jPanel4.setBackground(new java.awt.Color(51, 0, 102));

        btn_consulClientes.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        btn_consulClientes.setForeground(new java.awt.Color(255, 255, 255));
        btn_consulClientes.setText("          Consultar Clientes          >");
        btn_consulClientes.setToolTipText("");
        btn_consulClientes.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btn_consulClientes.setName(""); // NOI18N
        btn_consulClientes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_consulClientesMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_consulClientes, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_consulClientes, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel4);
        jPanel4.setBounds(0, 120, 170, 30);

        jPanel5.setBackground(new java.awt.Color(51, 0, 102));

        btn_consulProductos.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        btn_consulProductos.setForeground(new java.awt.Color(255, 255, 255));
        btn_consulProductos.setText("          Consultar Productos       >");
        btn_consulProductos.setToolTipText("");
        btn_consulProductos.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btn_consulProductos.setName(""); // NOI18N
        btn_consulProductos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_consulProductosMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_consulProductos, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_consulProductos, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel5);
        jPanel5.setBounds(0, 150, 170, 30);

        jPanel6.setBackground(new java.awt.Color(51, 0, 102));

        btn_agregarClientes.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        btn_agregarClientes.setForeground(new java.awt.Color(255, 255, 255));
        btn_agregarClientes.setText("          + Agregar Clientes          >");
        btn_agregarClientes.setToolTipText("");
        btn_agregarClientes.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btn_agregarClientes.setName(""); // NOI18N
        btn_agregarClientes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_agregarClientesMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_agregarClientes, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_agregarClientes, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel6);
        jPanel6.setBounds(0, 180, 170, 30);

        jPanel7.setBackground(new java.awt.Color(51, 0, 102));
        jPanel7.setLayout(null);

        btn_agregarProductos.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        btn_agregarProductos.setForeground(new java.awt.Color(255, 255, 255));
        btn_agregarProductos.setText("        + Agregar Productos       >");
        btn_agregarProductos.setToolTipText("");
        btn_agregarProductos.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btn_agregarProductos.setName(""); // NOI18N
        btn_agregarProductos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_agregarProductosMouseClicked(evt);
            }
        });
        jPanel7.add(btn_agregarProductos);
        btn_agregarProductos.setBounds(0, 10, 170, 15);

        btn_consultarFactuas.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        btn_consultarFactuas.setForeground(new java.awt.Color(255, 255, 255));
        btn_consultarFactuas.setText("         Cosultar Facturas            >");
        btn_consultarFactuas.setToolTipText("");
        btn_consultarFactuas.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btn_consultarFactuas.setName(""); // NOI18N
        btn_consultarFactuas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_consultarFactuasMouseClicked(evt);
            }
        });
        jPanel7.add(btn_consultarFactuas);
        btn_consultarFactuas.setBounds(0, 30, 170, 41);

        btn_consultarProformas.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        btn_consultarProformas.setForeground(new java.awt.Color(255, 255, 255));
        btn_consultarProformas.setText("         Cosultar Proformas         >");
        btn_consultarProformas.setToolTipText("");
        btn_consultarProformas.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btn_consultarProformas.setName(""); // NOI18N
        btn_consultarProformas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_consultarProformasMouseClicked(evt);
            }
        });
        jPanel7.add(btn_consultarProformas);
        btn_consultarProformas.setBounds(0, 60, 170, 41);

        jPanel1.add(jPanel7);
        jPanel7.setBounds(3, 210, 160, 130);

        btn_verDatosEmpresa.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        btn_verDatosEmpresa.setForeground(new java.awt.Color(255, 255, 255));
        btn_verDatosEmpresa.setText("          Datos Empresa          >");
        btn_verDatosEmpresa.setToolTipText("");
        btn_verDatosEmpresa.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btn_verDatosEmpresa.setName(""); // NOI18N
        btn_verDatosEmpresa.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_verDatosEmpresaMouseClicked(evt);
            }
        });
        jPanel1.add(btn_verDatosEmpresa);
        btn_verDatosEmpresa.setBounds(10, 10, 153, 15);

        jPanel2.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 170, 490));

        jLabel1.setFont(new java.awt.Font("Roboto", 1, 18)); // NOI18N
        jLabel1.setText("FACTURAR");
        jPanel2.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 10, -1, -1));
        jPanel2.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 30, 580, 10));

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel3.setLayout(null);

        tablaProductos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tablaProductos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaProductosMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tablaProductos);

        jPanel3.add(jScrollPane1);
        jScrollPane1.setBounds(10, 100, 570, 90);

        jLabel6.setText("Detalle de la factura (Moneda dólares americanos $)");
        jPanel3.add(jLabel6);
        jLabel6.setBounds(10, 10, 270, 14);

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));
        jPanel8.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel8.setLayout(null);

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(jTable2);

        jPanel8.add(jScrollPane2);
        jScrollPane2.setBounds(10, 150, 570, 80);

        jLabel11.setText("Detalle de la factura (Moneda dólares americanos $$)");
        jPanel8.add(jLabel11);
        jLabel11.setBounds(10, 10, 270, 14);

        jLabel12.setText("Datos del Comprador");
        jPanel8.add(jLabel12);
        jLabel12.setBounds(10, 110, 101, 14);

        jPanel3.add(jPanel8);
        jPanel8.setBounds(0, 0, 0, 0);

        jPanel11.setBackground(new java.awt.Color(255, 255, 255));
        jPanel11.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel11.setLayout(null);

        jTable5.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane5.setViewportView(jTable5);

        jPanel11.add(jScrollPane5);
        jScrollPane5.setBounds(10, 150, 570, 80);

        jLabel18.setText("Datos del Comprador");
        jPanel11.add(jLabel18);
        jLabel18.setBounds(10, 110, 101, 14);

        jPanel12.setBackground(new java.awt.Color(255, 255, 255));
        jPanel12.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel12.setLayout(null);

        jTable6.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane6.setViewportView(jTable6);

        jPanel12.add(jScrollPane6);
        jScrollPane6.setBounds(10, 150, 570, 80);

        jLabel19.setText("Detalle de la factura (Moneda dólares americanos $$)");
        jPanel12.add(jLabel19);
        jLabel19.setBounds(10, 10, 270, 14);

        jLabel20.setText("Datos del Comprador");
        jPanel12.add(jLabel20);
        jLabel20.setBounds(10, 110, 101, 14);

        jPanel11.add(jPanel12);
        jPanel12.setBounds(0, 0, 0, 0);

        jLabel21.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel21.setText("Cant.");
        jPanel11.add(jLabel21);
        jLabel21.setBounds(230, 10, 60, 15);

        jLabel22.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        jLabel22.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel22.setText("Descripción");
        jPanel11.add(jLabel22);
        jLabel22.setBounds(10, 10, 60, 15);

        jLabel23.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        jLabel23.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel23.setText("V.Unit.");
        jPanel11.add(jLabel23);
        jLabel23.setBounds(340, 10, 50, 15);
        jPanel11.add(txtValorUnitProducto);
        txtValorUnitProducto.setBounds(340, 30, 50, 30);
        jPanel11.add(txtvalorTotalProducto);
        txtvalorTotalProducto.setBounds(400, 30, 50, 30);

        jLabel24.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        jLabel24.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel24.setText("V.Total.");
        jPanel11.add(jLabel24);
        jLabel24.setBounds(400, 10, 50, 15);

        agregarProd.setText("OK");
        agregarProd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                agregarProdActionPerformed(evt);
            }
        });
        jPanel11.add(agregarProd);
        agregarProd.setBounds(513, 30, 47, 30);
        jPanel11.add(txtBuscaProduto);
        txtBuscaProduto.setBounds(10, 30, 180, 30);

        buscarProducto.setText("B");
        buscarProducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buscarProductoActionPerformed(evt);
            }
        });
        jPanel11.add(buscarProducto);
        buscarProducto.setBounds(200, 30, 40, 30);

        spinUnidadesProducto.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spinUnidadesProductoStateChanged(evt);
            }
        });
        jPanel11.add(spinUnidadesProducto);
        spinUnidadesProducto.setBounds(250, 30, 50, 30);

        jLabel4.setText("Iva a colocar:");
        jPanel11.add(jLabel4);
        jLabel4.setBounds(460, 10, 90, 14);

        txtIva.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtIvaMouseClicked(evt);
            }
        });
        jPanel11.add(txtIva);
        txtIva.setBounds(460, 30, 40, 30);

        jPanel3.add(jPanel11);
        jPanel11.setBounds(10, 30, 570, 70);

        jLabel25.setFont(new java.awt.Font("Roboto", 0, 10)); // NOI18N
        jLabel25.setText("Desarrollado por G2UNL - Derechos Reservados");
        jPanel3.add(jLabel25);
        jLabel25.setBounds(10, 290, 230, 14);

        txtSubTotalIva.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        txtSubTotalIva.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        jPanel3.add(txtSubTotalIva);
        txtSubTotalIva.setBounds(520, 200, 60, 19);

        jLabel26.setFont(new java.awt.Font("Roboto", 0, 10)); // NOI18N
        jLabel26.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel26.setText("SUB TOTAL 12% IVA $");
        jPanel3.add(jLabel26);
        jLabel26.setBounds(380, 200, 130, 14);

        jLabel27.setFont(new java.awt.Font("Roboto", 0, 10)); // NOI18N
        jLabel27.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel27.setText("SUB TOTAL $");
        jPanel3.add(jLabel27);
        jLabel27.setBounds(380, 220, 130, 14);

        txtSubTotal.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        txtSubTotal.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        jPanel3.add(txtSubTotal);
        txtSubTotal.setBounds(520, 220, 60, 20);

        jLabel28.setFont(new java.awt.Font("Roboto", 1, 10)); // NOI18N
        jLabel28.setText("FORMA DE PAGO");
        jPanel3.add(jLabel28);
        jLabel28.setBounds(10, 210, 130, 14);

        txtTotal.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        txtTotal.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        jPanel3.add(txtTotal);
        txtTotal.setBounds(520, 240, 60, 19);

        jLabel30.setFont(new java.awt.Font("Roboto", 1, 10)); // NOI18N
        jLabel30.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel30.setText("VALOR TOTAL $");
        jPanel3.add(jLabel30);
        jLabel30.setBounds(380, 240, 130, 14);

        radioOtros.setFont(new java.awt.Font("Roboto", 0, 10)); // NOI18N
        radioOtros.setText("Otros");
        jPanel3.add(radioOtros);
        radioOtros.setBounds(330, 230, 60, 23);

        radioEfectivo.setFont(new java.awt.Font("Roboto", 0, 10)); // NOI18N
        radioEfectivo.setText("Efectivo");
        radioEfectivo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioEfectivoActionPerformed(evt);
            }
        });
        jPanel3.add(radioEfectivo);
        radioEfectivo.setBounds(10, 230, 70, 23);

        radioCredito.setFont(new java.awt.Font("Roboto", 0, 10)); // NOI18N
        radioCredito.setText("T.Crédito/Débito");
        jPanel3.add(radioCredito);
        radioCredito.setBounds(80, 230, 110, 23);

        radioDepositoTransferencia.setFont(new java.awt.Font("Roboto", 0, 10)); // NOI18N
        radioDepositoTransferencia.setText("Depósito/Transferencia");
        jPanel3.add(radioDepositoTransferencia);
        radioDepositoTransferencia.setBounds(190, 230, 140, 23);

        Proformar.setText("Proformar");
        Proformar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ProformarActionPerformed(evt);
            }
        });
        jPanel3.add(Proformar);
        Proformar.setBounds(490, 280, 90, 23);

        Facturar.setText("Facturar");
        Facturar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FacturarActionPerformed(evt);
            }
        });
        jPanel3.add(Facturar);
        Facturar.setBounds(400, 280, 90, 23);

        jButton1.setText("Cancelar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton1);
        jButton1.setBounds(300, 280, 90, 23);

        jPanel2.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 170, 590, 310));

        jLabel5.setFont(new java.awt.Font("Roboto", 0, 10)); // NOI18N
        jLabel5.setText("TELEFONO MOVIL:");
        jPanel2.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 60, -1, -1));
        jPanel2.add(txtCelular, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 80, 100, -1));

        jLabel7.setFont(new java.awt.Font("Roboto", 0, 10)); // NOI18N
        jLabel7.setText("EMAIL:");
        jPanel2.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 110, -1, -1));

        txtEmail.setToolTipText("");
        jPanel2.add(txtEmail, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 130, 200, -1));
        jPanel2.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 160, 590, 10));

        jLabel9.setFont(new java.awt.Font("Roboto", 0, 10)); // NOI18N
        jLabel9.setText("DIRECCIÓN COMPRADOR:");
        jPanel2.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 110, -1, -1));

        jLabel10.setText("Datos del Comprador");
        jPanel2.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 40, -1, -1));
        jPanel2.add(txtDireccion, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 130, 370, -1));

        jLabel3.setText("Identificador:");
        jPanel2.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 80, 70, -1));
        jPanel2.add(txtCedula, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 70, 240, 30));

        btBuscar.setText("Buscar");
        btBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btBuscarActionPerformed(evt);
            }
        });
        jPanel2.add(btBuscar, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 70, -1, -1));

        getContentPane().add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 780, 490));

        setSize(new java.awt.Dimension(797, 528));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btn_consulClientesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_consulClientesMouseClicked
        // TODO add your handling code here:
        Frm_Persona nuevoConsulCliente = new Frm_Persona();
        nuevoConsulCliente.setVisible(true);
        factura.guardar(null);
        this.dispose();
    }//GEN-LAST:event_btn_consulClientesMouseClicked

    private void btn_consulProductosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_consulProductosMouseClicked
        // TODO add your handling code here:
        Frm_Productos nuevoConsulProductos = new Frm_Productos();
        nuevoConsulProductos.setVisible(true);
        factura.guardar(null);
        this.dispose();
    }//GEN-LAST:event_btn_consulProductosMouseClicked

    private void btn_agregarClientesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_agregarClientesMouseClicked
        // TODO add your handling code here:
        Frm_agregarPersona nuevoAgregarClientes = new Frm_agregarPersona();
        nuevoAgregarClientes.setVisible(true);
    }//GEN-LAST:event_btn_agregarClientesMouseClicked

    private void btn_agregarProductosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_agregarProductosMouseClicked
        // TODO add your handling code here:
        Frm_agregarProductos nuevoAgregarProductos = new Frm_agregarProductos();
        nuevoAgregarProductos.setVisible(true);
    }//GEN-LAST:event_btn_agregarProductosMouseClicked

    private void btn_cerrarSesionMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_cerrarSesionMouseClicked
        // TODO add your handling code here:
        Frm_vistaLogin nuevoLogin = new Frm_vistaLogin();
        nuevoLogin.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btn_cerrarSesionMouseClicked

    private void btn_cerrarSesionMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_cerrarSesionMouseEntered
        // TODO add your handling code here:
        panel_cerrarSesion.setBackground(Color.white);
        this.dispose();

    }//GEN-LAST:event_btn_cerrarSesionMouseEntered

    private void btn_cerrarSesionMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_cerrarSesionMouseExited
        // TODO add your handling code here:
        panel_cerrarSesion.setBackground(Color.blue);
        this.dispose();

    }//GEN-LAST:event_btn_cerrarSesionMouseExited

    private void radioEfectivoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioEfectivoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_radioEfectivoActionPerformed

    private void agregarProdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_agregarProdActionPerformed
        llenarTabla();
    }//GEN-LAST:event_agregarProdActionPerformed

    private void FacturarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FacturarActionPerformed
        guardar(true);
    }//GEN-LAST:event_FacturarActionPerformed

    private void ProformarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ProformarActionPerformed
        guardar(false);
    }//GEN-LAST:event_ProformarActionPerformed

    private void btBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btBuscarActionPerformed
        buscarCliente();
    }//GEN-LAST:event_btBuscarActionPerformed

    private void buscarProductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buscarProductoActionPerformed
        buscarProducto();
    }//GEN-LAST:event_buscarProductoActionPerformed

    private void txtIvaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtIvaMouseClicked
        modificarIva();
    }//GEN-LAST:event_txtIvaMouseClicked

    private void spinUnidadesProductoStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spinUnidadesProductoStateChanged
        selecionarUnidadesProducto();
    }//GEN-LAST:event_spinUnidadesProductoStateChanged

    private void btn_consultarFactuasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_consultarFactuasMouseClicked
        Frm_Facturas fact = new Frm_Facturas();
        fact.setVisible(true);
        factura.guardar(null);
        this.dispose();
    }//GEN-LAST:event_btn_consultarFactuasMouseClicked

    private void btn_consultarProformasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_consultarProformasMouseClicked
        Frm_Proformas prof = new Frm_Proformas();
        prof.setVisible(true);
        factura.guardar(null);
        this.dispose();
    }//GEN-LAST:event_btn_consultarProformasMouseClicked

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        limpiar();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void tablaProductosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaProductosMouseClicked
        editarCarrito();
    }//GEN-LAST:event_tablaProductosMouseClicked

    private void btn_verDatosEmpresaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_verDatosEmpresaMouseClicked
        Frm_DatosEmpresa emp = new Frm_DatosEmpresa();
        emp.setVisible(true);
        factura.guardar(null);
        this.dispose();
    }//GEN-LAST:event_btn_verDatosEmpresaMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Frm_vistaFactura.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Frm_vistaFactura.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Frm_vistaFactura.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Frm_vistaFactura.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Frm_vistaFactura().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Facturar;
    private javax.swing.JButton Proformar;
    private javax.swing.JButton agregarProd;
    private javax.swing.JButton btBuscar;
    private javax.swing.JLabel btn_agregarClientes;
    private javax.swing.JLabel btn_agregarProductos;
    private javax.swing.JLabel btn_cerrarSesion;
    private javax.swing.JLabel btn_consulClientes;
    private javax.swing.JLabel btn_consulProductos;
    private javax.swing.JLabel btn_consultarFactuas;
    private javax.swing.JLabel btn_consultarProformas;
    private javax.swing.JLabel btn_verDatosEmpresa;
    private javax.swing.JButton buscarProducto;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.ButtonGroup buttonGroup3;
    private javax.swing.ButtonGroup buttonGroup4;
    private javax.swing.ButtonGroup buttonGroup5;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JTable jTable2;
    private javax.swing.JTable jTable3;
    private javax.swing.JTable jTable4;
    private javax.swing.JTable jTable5;
    private javax.swing.JTable jTable6;
    private javax.swing.JPanel panel_cerrarSesion;
    private javax.swing.JRadioButton radioCredito;
    private javax.swing.JRadioButton radioDepositoTransferencia;
    private javax.swing.JRadioButton radioEfectivo;
    private javax.swing.JRadioButton radioOtros;
    private javax.swing.JSpinner spinUnidadesProducto;
    private javax.swing.JTable tablaProductos;
    private javax.swing.JTextField txtBuscaProduto;
    private javax.swing.JTextField txtCedula;
    private javax.swing.JTextField txtCelular;
    private javax.swing.JTextField txtDireccion;
    private javax.swing.JTextField txtEmail;
    private javax.swing.JTextField txtIva;
    private javax.swing.JTextField txtSubTotal;
    private javax.swing.JTextField txtSubTotalIva;
    private javax.swing.JTextField txtTotal;
    private javax.swing.JTextField txtValorUnitProducto;
    private javax.swing.JTextField txtvalorTotalProducto;
    // End of variables declaration//GEN-END:variables
}
