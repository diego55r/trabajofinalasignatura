/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import controlador.daos.DetalleFacturaDao;
import controlador.daos.ProductoDao;
import java.awt.Color;
import javax.swing.JOptionPane;
import lista.controlador.Lista;
import modelo.Factura;
import modelo.Producto;
import vista.modelo.tablaProducto;

/**
 *
 * @author Omar Maldonado
 */
public class Frm_Productos extends javax.swing.JFrame {

    /**
     * Creates new form Frm_Productos
     */
    private ProductoDao productodao= new ProductoDao();
    private DetalleFacturaDao facturaDao= new DetalleFacturaDao();
    private tablaProducto mtc = new tablaProducto();
    
    
    /**
    *Se inicializa el la ventana
    */
    public Frm_Productos() {
        initComponents();
        limpiar();
        
    }
    /**
    *Carga tabla el la ventana
    */
    private void cargarTabla(){
        
        
        mtc.setLista(productodao.soloVisibles());
        tblProducto.setModel(mtc);
        tblProducto.updateUI();
    }
    
     /**
    *Limpia tabla el la ventana
    */
    
    private void limpiar(){
        cargarTabla();
        txt_codigo.setText("");
        txt_nombreProducto.setText("");
        
    }
   
    /**
    *Metodo para agregar producto
    */
    
    private void cargarAgregarProducto() {
        int fila = tblProducto.getSelectedRow();
        if (fila >= 0) {
        if (productodao.soloVisibles().consultarDatoPosicion(fila) != null) {
                productodao.setProducto(mtc.getLista().consultarDatoPosicion(fila));
                productodao.getProducto().setId(((Producto)productodao.getLista().consultarDatoPosicion(fila)).getId());
                Frm_agregarProductos frmAgregarProducto = new Frm_agregarProductos(productodao, fila);
                frmAgregarProducto.setVisible(true);
               }else{
                System.out.println("NO CARGA");
                System.out.println(mtc.getLista().consultarDatoPosicion(fila));
            }
        }
    }
     /**
    *Metodo para cargar celdas ya eliminadas
    */
    private void cargarEliminar() {
        int fila = tblProducto.getSelectedRow();
        if (fila >= 0) {
            if (productodao.soloVisibles().consultarDatoPosicion(fila) != null) {
                productodao.setProducto(mtc.getLista().consultarDatoPosicion(fila));
                productodao.getProducto().setId(((Producto)productodao.soloVisibles().consultarDatoPosicion(fila)).getId());
                Frm_agregarProductos frmAgregarProducto = new Frm_agregarProductos(productodao, fila,false);
                frmAgregarProducto.setVisible(true);
               }else{
                System.out.println("NO CARGA");
                System.out.println(mtc.getLista().consultarDatoPosicion(fila));
            }
        }
    }
     /**
    *Metodo para buscar productos
    */
    private void buscar(){
    
        Lista aux= new Lista();
        if(txt_nombreProducto.isFocusOwner()){
           
            aux=productodao.buscarString(txt_nombreProducto.getText());
        } else if (txt_codigo.isFocusOwner()){
            aux=productodao.buscarCodigo(txt_codigo.getText());
        } else {
            aux=mtc.getLista();
        }
            

        mtc.setLista(aux);
        tblProducto.setModel(mtc);
        tblProducto.updateUI();
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblProducto = new javax.swing.JTable();
        txt_codigo = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txt_nombreProducto = new javax.swing.JTextField();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel5 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        btn_agregarProducto = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        btn_buscar = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        btn_irFacturacion = new javax.swing.JLabel();
        jPanel11 = new javax.swing.JPanel();
        btn_Modificar = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        btn_Eliminar = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Roboto", 1, 14)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel1.setText("Catálogo de Productos");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 120, 180, 20));
        jPanel1.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 110, 760, 10));

        jLabel2.setFont(new java.awt.Font("Roboto", 1, 18)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("PRODUCTOS");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 10, 110, 20));

        tblProducto.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblProducto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblProductoMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblProducto);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 150, 740, 250));

        txt_codigo.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        txt_codigo.setForeground(new java.awt.Color(204, 204, 204));
        txt_codigo.setText("Ingrese el código del producto");
        txt_codigo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txt_codigoMouseClicked(evt);
            }
        });
        txt_codigo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_codigoActionPerformed(evt);
            }
        });
        jPanel1.add(txt_codigo, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 80, 170, -1));

        jLabel3.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        jLabel3.setText("Código:");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 60, -1, -1));

        jLabel4.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        jLabel4.setText("Nombre:");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 60, -1, -1));

        txt_nombreProducto.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        txt_nombreProducto.setForeground(new java.awt.Color(204, 204, 204));
        txt_nombreProducto.setText("Ingrese el nombre del producto");
        txt_nombreProducto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txt_nombreProductoMouseClicked(evt);
            }
        });
        jPanel1.add(txt_nombreProducto, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 80, 170, -1));
        jPanel1.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, 760, 10));

        jLabel5.setFont(new java.awt.Font("Roboto", 1, 14)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel5.setText("Búsqueda");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, 110, 20));

        jPanel6.setBackground(new java.awt.Color(0, 102, 255));

        btn_agregarProducto.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        btn_agregarProducto.setForeground(new java.awt.Color(255, 255, 255));
        btn_agregarProducto.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btn_agregarProducto.setText("+ Agregar Producto");
        btn_agregarProducto.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_agregarProducto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_agregarProductoMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_agregarProducto, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_agregarProducto, javax.swing.GroupLayout.DEFAULT_SIZE, 42, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 60, 110, -1));

        jPanel7.setBackground(new java.awt.Color(0, 102, 255));
        jPanel7.setForeground(new java.awt.Color(0, 102, 255));

        btn_buscar.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        btn_buscar.setForeground(new java.awt.Color(255, 255, 255));
        btn_buscar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btn_buscar.setText("Buscar Producto");
        btn_buscar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_buscar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_buscarMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_buscar, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_buscar, javax.swing.GroupLayout.DEFAULT_SIZE, 42, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 60, 110, -1));

        jPanel8.setBackground(new java.awt.Color(0, 102, 255));

        btn_irFacturacion.setFont(new java.awt.Font("Roboto", 1, 14)); // NOI18N
        btn_irFacturacion.setForeground(new java.awt.Color(255, 255, 255));
        btn_irFacturacion.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btn_irFacturacion.setText("FACTURACIÓN");
        btn_irFacturacion.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_irFacturacion.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_irFacturacionMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_irFacturacion, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_irFacturacion, javax.swing.GroupLayout.DEFAULT_SIZE, 42, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 60, 120, -1));

        jPanel11.setBackground(new java.awt.Color(0, 102, 255));

        btn_Modificar.setFont(new java.awt.Font("Roboto", 1, 14)); // NOI18N
        btn_Modificar.setForeground(new java.awt.Color(255, 255, 255));
        btn_Modificar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btn_Modificar.setText("MODIFICAR");
        btn_Modificar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_Modificar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_ModificarMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_Modificar, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_Modificar, javax.swing.GroupLayout.DEFAULT_SIZE, 42, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 420, -1, -1));

        jPanel10.setBackground(new java.awt.Color(0, 102, 255));

        btn_Eliminar.setFont(new java.awt.Font("Roboto", 1, 14)); // NOI18N
        btn_Eliminar.setForeground(new java.awt.Color(255, 255, 255));
        btn_Eliminar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btn_Eliminar.setText("ELIMINAR");
        btn_Eliminar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_Eliminar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_EliminarMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_Eliminar, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_Eliminar, javax.swing.GroupLayout.DEFAULT_SIZE, 42, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 420, -1, -1));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 780, 490));

        setSize(new java.awt.Dimension(796, 528));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void txt_codigoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_codigoActionPerformed
        // TODO add your handling code here:
        
    }//GEN-LAST:event_txt_codigoActionPerformed
     /**
    *txt que ingresa los valores a buscar por código
    */
    
    private void txt_codigoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txt_codigoMouseClicked
        // TODO add your handling code here:
        txt_codigo.setText("");
        txt_codigo.setForeground(Color.blue);
        txt_nombreProducto.setText("Ingrese el nombre del producto");
        
        
    }//GEN-LAST:event_txt_codigoMouseClicked
     /**
    *txt que ingresa los valores a buscar por nombre
    */
    
    private void txt_nombreProductoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txt_nombreProductoMouseClicked
        // TODO add your handling code here:
        txt_nombreProducto.setText("");
        txt_nombreProducto.setForeground(Color.blue);
        txt_codigo.setText("Ingrese el código del producto");
        
    }//GEN-LAST:event_txt_nombreProductoMouseClicked
     /**
    *Botón que da paso a la ventana agregar productos
    */
    
    private void btn_agregarProductoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_agregarProductoMouseClicked
        // TODO add your handling code here:
        Frm_agregarProductos nuevoProducto = new Frm_agregarProductos();
        nuevoProducto.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btn_agregarProductoMouseClicked
     /**
    *Botón que da paso a buscar
    */
    private void btn_buscarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_buscarMouseClicked
        // TODO add your handling code here:
        buscar();
    }//GEN-LAST:event_btn_buscarMouseClicked
    
    private void tblProductoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblProductoMouseClicked
        // TODO add your handling code here:
      
    }//GEN-LAST:event_tblProductoMouseClicked
     /**
    *Botón que permite regresar a la facturación
    */
    private void btn_irFacturacionMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_irFacturacionMouseClicked
        // TODO add your handling code here:
        Frm_vistaFactura nuevaFactura = new Frm_vistaFactura();
        nuevaFactura.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btn_irFacturacionMouseClicked

    private void btn_EliminarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_EliminarMouseClicked
        // TODO add your handling code here:
        cargarEliminar();
        cargarTabla();
        this.dispose();
    }//GEN-LAST:event_btn_EliminarMouseClicked

    private void btn_ModificarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_ModificarMouseClicked
        // TODO add your handling code here:
       cargarAgregarProducto();
       cargarTabla();
       this.dispose();
             
       
    }//GEN-LAST:event_btn_ModificarMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Frm_Productos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Frm_Productos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Frm_Productos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Frm_Productos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Frm_Productos().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel btn_Eliminar;
    private javax.swing.JLabel btn_Modificar;
    private javax.swing.JLabel btn_agregarProducto;
    private javax.swing.JLabel btn_buscar;
    private javax.swing.JLabel btn_irFacturacion;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTable tblProducto;
    private javax.swing.JTextField txt_codigo;
    private javax.swing.JTextField txt_nombreProducto;
    // End of variables declaration//GEN-END:variables
}
