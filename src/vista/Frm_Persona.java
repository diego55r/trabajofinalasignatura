/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import controlador.daos.CuentaDao;
import controlador.daos.PersonaDao;
import java.awt.Color;
import lista.controlador.Lista;
import modelo.Persona;
import vista.modelo.tablaPersona;

/**
 *
 * @author santy
 */
public class Frm_Persona extends javax.swing.JFrame {

    /**
     * Creates new form Frm_Productos
     */
    private PersonaDao personadao = new PersonaDao();
    private tablaPersona mtc = new tablaPersona();

    public Frm_Persona() {
        initComponents();
        limpiar();
    }

    private void cargarTabla() {
        mtc.setLista(personadao.listar());
        tblCliente.setModel(mtc);
        mtc.fireTableStructureChanged();
        tblCliente.updateUI();
    }

    private void limpiar() {
        cargarTabla();
        txt_cedulaCliente.setText("");
        txt_nombreCliente.setText("");
    }

    /*
    *  Este método de buscar(), nos ayuda a buscar en la lista, tanto: nombre como cedula
    * ingresados por el usuario 
     */
    public void buscar() {
        mtc.setLista(personadao.listar());
        Lista cargar = new Lista();
        if (txt_nombreCliente.isFocusOwner()) {

            cargar = personadao.buscarString(txt_nombreCliente.getText());
        } else if (txt_cedulaCliente.isFocusOwner()) {
            System.out.println("Si entro");
            cargar = personadao.buscarCedula(txt_cedulaCliente.getText());
        } else {
            cargar = mtc.getLista();
        }

        mtc.setLista(cargar);
        tblCliente.setModel(mtc);
        mtc.fireTableStructureChanged();
        tblCliente.updateUI();
    }

    /**
     * nos permite cargar la vista de agregar Personas,y carga los datos de persona en los jtextfield correspondientes
     * para poder realizar la actualizacion
     */
    private void cargarAgregarPersona() {
        int fila = tblCliente.getSelectedRow();
        if (fila >= 0) {
            if (personadao.getLista().consultarDatoPosicion(fila) != null) {
                System.out.println("getPersona Dao " + personadao.getLista().consultarDatoPosicion(fila));
                personadao.setPersona(mtc.getLista().consultarDatoPosicion(fila));
                personadao.getPersona().setId(Long.parseLong(personadao.getPersona().getIdentificador()));
                Frm_agregarPersona frmAgregarCliente = new Frm_agregarPersona(personadao, fila);
                frmAgregarCliente.setVisible(true);
                frmAgregarCliente.btn_guardarPersona.setVisible(false);
                frmAgregarCliente.btn_guardarPersona.setText("No disponible");
            } else {
                System.out.println("NO CARGA");
                System.out.println(mtc.getLista().consultarDatoPosicion(fila));
            }
        }
    }

    /*
    * Este método borra la fila seleccionada, utilizamos el getSelectedRow para darle el valor
    * de la fila seleccionada a una variable int, y despues hacemos la eliminación con la posicion en la lista
     */
    private void borrarRow() {
        int fila = tblCliente.getSelectedRow();
        personadao.setPersona(mtc.getLista().consultarDatoPosicion(fila));
        if (fila >= 0) {
            if (personadao.getLista().consultarDatoPosicion(fila) != null) {
                System.out.println("getPersona Eliminar Dao " + personadao.getLista().consultarDatoPosicion(fila));
                personadao.getLista().eliminarPorPosicion(fila);
                System.out.println("acutalizar listsa");
                personadao.actualizarDatos(fila);
                System.out.println("supuestamente actualiza");
                System.out.println(personadao.getPersona().getId());
                System.out.println(personadao.getPersona().getNombre());
                mtc.fireTableStructureChanged();
                tblCliente.updateUI();
            } else {
                System.out.println("NO CARGA");
                System.out.println(mtc.getLista().consultarDatoPosicion(fila));
            }
        }
    }

//    private void ordenar() {
//        int select = cbxSeleccionar.getSelectedIndex();
//        int opcion = cbxCriteriosB.getSelectedIndex();
//        String criterio = "Apellido";
//        if (opcion > 0) {
//            criterio = (opcion == 1) ? "nombres" : "edad";
//        }
//
//        System.out.println("seleccionar cbx" + select);
//        if (select == 0) {
//            cargarTabla();
//        } else if (select == 1) {
//            Lista aux = personadao.listar().seleccion_clase(criterio, Lista.ASCENDETE);
//            mtc.setLista(aux);
//            tblCliente.setModel(mtc);
//        } else {
//            Lista aux = personadao.listar().seleccion_clase(criterio, Lista.DESCENDETE);
//            mtc.setLista(aux);
//            tblCliente.setModel(mtc);
//        }
//        tblCliente.updateUI();
//    }
    /*
    *El método ordenar(), nos permite ordenar ascendente y descendente la lista esto es tomando en cuenta el apellido
     */
    private void ordenar() {
        int select = cbxSeleccionar.getSelectedIndex();
        if (select == 0) {
            cargarTabla();
        } else if (select == 1) {
            Lista aux = personadao.listar().qsortString(0, personadao.listar().tamanio() - 1, Lista.ASCENDETE);
            mtc.setLista(aux);
            tblCliente.setModel(mtc);
        } else {
            Lista aux = personadao.listar().qsortString(0, personadao.listar().tamanio() - 1, Lista.DESCENDETE);
            mtc.setLista(aux);
            tblCliente.setModel(mtc);
        }
        tblCliente.updateUI();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblCliente = new javax.swing.JTable();
        txt_nombreCliente = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txt_cedulaCliente = new javax.swing.JTextField();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel5 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        btn_agregarClientes = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        btn_buscarCliente = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        btn_irFacturacion = new javax.swing.JLabel();
        jPanel9 = new javax.swing.JPanel();
        btn_buscarCliente1 = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        btn_buscarCliente3 = new javax.swing.JLabel();
        jPanel11 = new javax.swing.JPanel();
        btn_buscarCliente2 = new javax.swing.JLabel();
        cbxSeleccionar = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Roboto", 1, 14)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel1.setText("Listado de Clientes");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 120, 180, 20));
        jPanel1.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 110, 760, 10));

        jLabel2.setFont(new java.awt.Font("Roboto", 1, 18)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("CLIENTES");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 10, 110, 20));

        tblCliente.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblCliente);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 150, 740, 320));

        txt_nombreCliente.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        txt_nombreCliente.setForeground(new java.awt.Color(204, 204, 204));
        txt_nombreCliente.setText("Ingrese el código del producto");
        txt_nombreCliente.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txt_nombreClienteMouseClicked(evt);
            }
        });
        txt_nombreCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_nombreClienteActionPerformed(evt);
            }
        });
        jPanel1.add(txt_nombreCliente, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 80, 170, -1));

        jLabel3.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        jLabel3.setText("Nombre:");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 60, -1, -1));

        jLabel4.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        jLabel4.setText("Cédula:");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 60, -1, -1));

        txt_cedulaCliente.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        txt_cedulaCliente.setForeground(new java.awt.Color(204, 204, 204));
        txt_cedulaCliente.setText("Ingrese el nombre del producto");
        txt_cedulaCliente.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txt_cedulaClienteMouseClicked(evt);
            }
        });
        jPanel1.add(txt_cedulaCliente, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 80, 170, -1));
        jPanel1.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, 760, 10));

        jLabel5.setFont(new java.awt.Font("Roboto", 1, 14)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel5.setText("Búsqueda");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, 110, 20));

        jPanel6.setBackground(new java.awt.Color(0, 102, 255));

        btn_agregarClientes.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        btn_agregarClientes.setForeground(new java.awt.Color(255, 255, 255));
        btn_agregarClientes.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btn_agregarClientes.setText("+ Agregar Clientes");
        btn_agregarClientes.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btn_agregarClientes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_agregarClientesMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_agregarClientes, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_agregarClientes, javax.swing.GroupLayout.DEFAULT_SIZE, 42, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 60, 110, -1));

        jPanel7.setBackground(new java.awt.Color(0, 102, 255));
        jPanel7.setForeground(new java.awt.Color(0, 102, 255));

        btn_buscarCliente.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        btn_buscarCliente.setForeground(new java.awt.Color(255, 255, 255));
        btn_buscarCliente.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btn_buscarCliente.setText("Buscar Cliente");
        btn_buscarCliente.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btn_buscarCliente.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_buscarClienteMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_buscarCliente, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_buscarCliente, javax.swing.GroupLayout.DEFAULT_SIZE, 42, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 60, 110, -1));

        jPanel8.setBackground(new java.awt.Color(0, 102, 255));

        btn_irFacturacion.setFont(new java.awt.Font("Roboto", 1, 14)); // NOI18N
        btn_irFacturacion.setForeground(new java.awt.Color(255, 255, 255));
        btn_irFacturacion.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btn_irFacturacion.setText("FACTURACIÓN");
        btn_irFacturacion.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btn_irFacturacion.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_irFacturacionMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_irFacturacion, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_irFacturacion, javax.swing.GroupLayout.DEFAULT_SIZE, 42, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 60, 120, -1));

        jPanel9.setBackground(new java.awt.Color(0, 102, 255));
        jPanel9.setForeground(new java.awt.Color(0, 102, 255));

        btn_buscarCliente1.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        btn_buscarCliente1.setForeground(new java.awt.Color(255, 255, 255));
        btn_buscarCliente1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btn_buscarCliente1.setText("Seleccionar");
        btn_buscarCliente1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btn_buscarCliente1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_buscarCliente1MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_buscarCliente1, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_buscarCliente1, javax.swing.GroupLayout.DEFAULT_SIZE, 42, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 480, -1, -1));

        jPanel10.setBackground(new java.awt.Color(0, 102, 255));
        jPanel10.setForeground(new java.awt.Color(0, 102, 255));

        btn_buscarCliente3.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        btn_buscarCliente3.setForeground(new java.awt.Color(255, 255, 255));
        btn_buscarCliente3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btn_buscarCliente3.setText("Borrar Cliente");
        btn_buscarCliente3.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btn_buscarCliente3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_buscarCliente3MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addComponent(btn_buscarCliente3, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btn_buscarCliente3, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel1.add(jPanel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 480, 110, -1));

        jPanel11.setBackground(new java.awt.Color(0, 102, 255));
        jPanel11.setForeground(new java.awt.Color(0, 102, 255));
        jPanel11.setLayout(null);

        btn_buscarCliente2.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        btn_buscarCliente2.setForeground(new java.awt.Color(255, 255, 255));
        btn_buscarCliente2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btn_buscarCliente2.setText("Ordenar Apellido");
        btn_buscarCliente2.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btn_buscarCliente2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_buscarCliente2MouseClicked(evt);
            }
        });
        jPanel11.add(btn_buscarCliente2);
        btn_buscarCliente2.setBounds(0, 0, 110, 42);

        cbxSeleccionar.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Ninguna", "Ascendente", "Descendente" }));
        cbxSeleccionar.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbxSeleccionarItemStateChanged(evt);
            }
        });
        jPanel11.add(cbxSeleccionar);
        cbxSeleccionar.setBounds(110, 10, 130, 25);

        jPanel1.add(jPanel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 480, 250, 40));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 780, 530));

        setSize(new java.awt.Dimension(796, 576));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void txt_nombreClienteMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txt_nombreClienteMouseClicked
        // TODO add your handling code here:
        txt_nombreCliente.setText("");
        txt_nombreCliente.setForeground(Color.blue);
        txt_cedulaCliente.setText("Ingrese el nombre del producto");

    }//GEN-LAST:event_txt_nombreClienteMouseClicked

    private void txt_nombreClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_nombreClienteActionPerformed
        // TODO add your handling code here:

    }//GEN-LAST:event_txt_nombreClienteActionPerformed

    private void txt_cedulaClienteMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txt_cedulaClienteMouseClicked
        // TODO add your handling code here:
        txt_cedulaCliente.setText("");
        txt_cedulaCliente.setForeground(Color.blue);
        txt_nombreCliente.setText("Ingrese el código del producto");

    }//GEN-LAST:event_txt_cedulaClienteMouseClicked

    private void btn_agregarClientesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_agregarClientesMouseClicked
        // TODO add your handling code here:
        Frm_agregarPersona nuevoCliente = new Frm_agregarPersona();
        nuevoCliente.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btn_agregarClientesMouseClicked

    private void btn_buscarClienteMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_buscarClienteMouseClicked
        // TODO add your handling code here:
        buscar();
    }//GEN-LAST:event_btn_buscarClienteMouseClicked

    private void btn_irFacturacionMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_irFacturacionMouseClicked
        // TODO add your handling code here:
        Frm_vistaFactura nuevaFactura = new Frm_vistaFactura();
        nuevaFactura.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btn_irFacturacionMouseClicked

    private void btn_buscarCliente1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_buscarCliente1MouseClicked
        // TODO add your handling code here:
        cargarAgregarPersona();
    }//GEN-LAST:event_btn_buscarCliente1MouseClicked

    private void btn_buscarCliente2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_buscarCliente2MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_buscarCliente2MouseClicked

    private void btn_buscarCliente3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_buscarCliente3MouseClicked
        // TODO add your handling code here:
        borrarRow();
    }//GEN-LAST:event_btn_buscarCliente3MouseClicked

    private void cbxSeleccionarItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbxSeleccionarItemStateChanged
        // TODO add your handling code here:
        ordenar();
    }//GEN-LAST:event_cbxSeleccionarItemStateChanged

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Frm_Persona.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Frm_Persona.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Frm_Persona.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Frm_Persona.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Frm_Persona().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel btn_agregarClientes;
    private javax.swing.JLabel btn_buscarCliente;
    private javax.swing.JLabel btn_buscarCliente1;
    private javax.swing.JLabel btn_buscarCliente2;
    private javax.swing.JLabel btn_buscarCliente3;
    private javax.swing.JLabel btn_irFacturacion;
    private javax.swing.JComboBox<String> cbxSeleccionar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTable tblCliente;
    private javax.swing.JTextField txt_cedulaCliente;
    private javax.swing.JTextField txt_nombreCliente;
    // End of variables declaration//GEN-END:variables
}
